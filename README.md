# Dan Herrera - Programming Practice Portfolio #
# README #

### Purpose ###

A public portfolio of solutions to competitive programming problems across the web for personal and public reference.

### Use Requests ###

Anyone is free to pull source code and other files from here for their own reference. I don't particularly care if anyone plagiarizes these files, that demon is left for you to handle; some recognition would be nice, but totally unneeded.

Some of the code here is a hybridization of my own implementation and style with code found through simple google searches, though nothing like full projects, simply misc algorithm implementations or code oddities; there is some code straight from Competitive Programming 3 which is copied as canned code for initial reference.

### Contact ###

For information or questions: **dherr060@fiu.edu**