This problem is a regular palindrome checking problem. After converting the characters to numbers, then checking the result, one gets a simple solution.

The solution is O(2N) where N is the length of the input string.

This was mainly practice for C++.