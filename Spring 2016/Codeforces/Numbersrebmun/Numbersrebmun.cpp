#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define eps (1.0e-9)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef pair<double,double> dd;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

class point {
public:
	double x, y;
	point() {
		x = 0;
		y = 0;
	}
	point(double x, double y) {
		this->x = x;;
		this->y = y;
	};
};

double dist(point a, point b) {
	return hypot(a.x - b.x, a.y - b.y);
}

bool circleContains(point center, double rad, point target) {
	return dist(target, center) <= rad + eps;
}

typedef vector<point> vpp;

bool circle2PtsRad(point p1, point p2, double r, point &c) {
	double d2 = (p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y);
	double det = r * r / d2 - 0.25;
	if (det < 0.0) return false;
	double h = sqrt(det);
	c.x = (p1.x + p2.x) * 0.5 + (p1.y - p2.y) * h;
	c.y = (p1.y + p2.y) * 0.5 + (p2.x - p1.x) * h;
	return true;
}

int main(){
	FASTER;

	vpp houses;

	double x, y;
	point c;
	int ans = 1;

	while (cin >> x >> y) {
		houses.push_back(point(x, y));
	}

	for (int i = 0; i < houses.size(); ++i) {
		for (int j = i + 1; j < houses.size(); j++) {
//			printf("debug %d %d\n",i,j);
			if (!circle2PtsRad(houses[i], houses[j], 2.5, c)) {
				continue;
			}
//			printf("debug2 %d %d\n",i,j);

			int temp = 0;

			for (int k = 0; k < houses.size(); ++k) {
				if (circleContains(c, 2.5, houses[k])) {
					temp++;
				}
			}

			ans = max(ans, temp);

			circle2PtsRad(houses[j], houses[i], 2.5, c);

			temp = 0;

			for (int k = 0; k < houses.size(); ++k) {

				if (circleContains(c, 2.5, houses[k])) {
					temp++;
				}
			}

			ans = max(ans, temp);
		}
	}

	cout << ans << endl;

	return 0;
}