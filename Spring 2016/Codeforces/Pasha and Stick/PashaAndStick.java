import java.util.*;

public class PashaAndStick {
	
	int n, res;
	
	public PashaAndStick() {
		Scanner in = new Scanner(System.in);
		
		n = in.nextInt();
		
		res = n % 2 == 1 ? 0 : ((n /= 2) / 2);
		
		res -= n % 2 == 0 ? 1 : 0;
		
		res = Math.max(res, 0);
		
		System.out.println(res);
		
		in.close();
	}
	
	public static void main(String[] args) {
		new PashaAndStick();
	}
	
}