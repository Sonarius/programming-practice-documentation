
#include <iostream>

using namespace std;

int main () {
	
	int n, res;
	
	cin >> n;
	
	if (n % 2) {
		cout << 0 << endl;
	} else {
		n /= 2;
		res = n / 2;
		
		if (n % 2 == 0) {
			res--;
		}
		
		cout << res << endl;
	}
	
	return 0;
}

