import java.util.*;
import java.io.*;

public class PaperRoute {
	
	boolean debugging = true;
	
	Scanner in;
	BufferedReader br;
	StringTokenizer st;
	StringBuilder sb;
	
   int n, graphWeight, finalRoutePath; 
   ArrayList<Node> houses;
   
   public void bfs() {
      Queue<Node> q = new LinkedList<Node>();
      houses.get(0).dist = 0;
      q.add(houses.get(0));
      
      while (!q.isEmpty()) {
         Node house = q.poll();
         ArrayList<Edge> roads = house.edges;
         finalRoutePath = Math.min(finalRoutePath, house.distToLoo - house.dist);
         for (Edge e : roads) {
            if (e.dest == house.parent) continue;
            houses.get(e.dest).parent = house.id;
            houses.get(e.dest).dist = house.dist + e.weight;
            q.add(houses.get(e.dest));
         }
      }
   }
   
   public void init() {
      n = in.nextInt() + 1;
      graphWeight = 0;
      finalRoutePath = Integer.MAX_VALUE;
      houses = new ArrayList<Node>(n);
      for (int i = 0; i < n; i++) {
         houses.add(new Node(i));
         houses.get(i).distToLoo = in.nextInt();
      }
      for (int i = 0; i < n - 1; i++) {
         int u = in.nextInt(), v = in.nextInt(), w = in.nextInt();
         // Aren't garunteed that new Edges will be connected
         // to previously connected Houses, so every new Edge
         // must be bi-directional and bfs will have to keep
         // track of not visiting the caller of the current
         // House
         houses.get(u).edges.add(new Edge(u, v, w));
         houses.get(v).edges.add(new Edge(v, u, w));
         graphWeight += w;
      }
   }
   
	public PaperRoute() {
		in = new Scanner(System.in);
      init();
      bfs();
      System.out.println(graphWeight * 2 + finalRoutePath);
      in.close();
	}
	
	public static void main(String[] args) {
		new PaperRoute();
	}
	
}

class Node {
   
   public int id, dist, distToLoo, parent;
   public ArrayList<Edge> edges;
   
   public Node (int id) {
      this.id = id;
      this.dist = Integer.MAX_VALUE;
      this.distToLoo = -1;
      this.parent = -1;
      edges = new ArrayList<Edge>();
   }
   
}

class Edge {
   
   public int source, dest, weight;
   
   public Edge (int source, int dest, int weight) {
      this.source = source;
      this.dest = dest;
      this.weight = weight;
   }
   
}
