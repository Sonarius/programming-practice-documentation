// Adapted from Judge's Solution in Waterloo Online Contest History

#include <bits/stdc++.h>
using namespace std;

const int MAX_INT = std::numeric_limits<int>::max();
const int MIN_INT = std::numeric_limits<int>::min();
const int INF = 1.0e9;
const int NEG_INF = -1.0e9;

#define MEM(arr,val)memset(arr,val, sizeof arr)
#define PI acos(0)*2.0
#define eps 1.0e-9
#define are_equal(a,b)fabs(a-b)<eps
#define LS(b)(b& (-b)) // Least significant bit
#define DEG_to_RAD(a)((a*PI)/180.0) // convert to radians

#define forn(n) for(int i = 0; i < (n); i++)

#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef pair<int,char> ic;
typedef pair<long,char> lc;
typedef vector<int> vi;
typedef vector<ii> vii;

int gcd(int a,int b){return b == 0 ? a : gcd(b,a%b);}
int lcm(int a,int b){return a*(b/gcd(a,b));}

const int MAX_N = 1.0e5 + 5;

int n = 0, weightSum = 0, minRoute = INF;
int distToLoo[MAX_N];
vii edgesOf[MAX_N];

void search(int source, int Psource, int dist) {
   minRoute = min(minRoute, distToLoo[source] - dist);
   vii edges = edgesOf[source];
   forn(edges.size()) {
      if (edges[i].first == Psource) continue;
      search(edges[i].first, source, dist + edges[i].second);
   }
}

int main() {
	FASTER;
	
	cin >> n;
   
   n++;
   forn(n) {
      cin >> distToLoo[i];
   }
   
   n--;
   forn(n) {
      int u, v, w;
      cin >> u >> v >> w;
      edgesOf[u].push_back(make_pair(v, w));
      edgesOf[v].push_back(make_pair(u, w));
      weightSum += w;
   }
   
   search(0, 0, 0);
   
   cout << (2 * weightSum + minRoute) << endl;
	
	return 0;
}