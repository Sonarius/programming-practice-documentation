#include <bits/stdc++.h>
using namespace std;

const int MAX_INT = std::numeric_limits<int>::max();
const int MIN_INT = std::numeric_limits<int>::min();
const int INF = 1.0e9;
const int NEG_INF = -1.0e9;

#define max(a,b)(a>b?a:b)
#define min(a,b)(a<b?a:b)
#define MEM(arr,val)memset(arr,val, sizeof arr)
#define PI acos(0)*2.0
#define eps 1.0e-9
#define are_equal(a,b)fabs(a-b)<eps
#define LS(b)(b& (-b)) // Least significant bit
#define DEG_to_RAD(a)((a*PI)/180.0) // convert to radians

#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef pair<int,char> ic;
typedef pair<long,char> lc;
typedef vector<int> vi;
typedef vector<ii> vii;

int gcd(int a,int b){return b == 0 ? a : gcd(b,a%b);}
int lcm(int a,int b){return a*(b/gcd(a,b));}

int main() {
	FASTER;	
	
	int a, b, c, x, y, z, l, m, n, q, r, s;
	
	cin >> a >> b >> c >> x >> y >> z;
	
	l = max(0, (x - a));
	m = max(0, (y - b));
	n = max(0, (z - c));
	
	q = max(0, (a - x));
	r = max(0, (b - y));
	s = max(0, (c - z));
	
	q /= 2; r /= 2; s /= 2;
	
	bool pass = (q + r + s) >= (l + m + n);
	
	cout << (pass ? "Yes" : "No") << endl;
	
	return 0;
}