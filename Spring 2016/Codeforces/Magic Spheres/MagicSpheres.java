import java.util.Scanner;
import java.util.StringTokenizer;
import java.io.BufferedReader;

public class MagicSpheres {
	
	boolean debugging;
	
	Scanner in;
	BufferedReader br;
	StringTokenizer st;
	StringBuilder sb;
	
	int a, b, c; // Current balls
	int x, y, z; // Needed balls
	int q, r, s; // Lacking balls
	int l, m, n; // Extra balls
	
	public MagicSpheres() {
		in = new Scanner(System.in);
		
		a = in.nextInt();
		b = in.nextInt();
		c = in.nextInt();
		
		x = in.nextInt();
		y = in.nextInt();
		z = in.nextInt();
		
		System.out.println(solve() ? "Yes" : "No");
		
		in.close();
	}
	
	public boolean solve() {
		q = Math.max(x - a, 0);
		r = Math.max(y - b, 0);
		s = Math.max(z - c, 0);
		
		l = Math.max(a - x, 0);
		m = Math.max(b - y, 0);
		n = Math.max(c - z, 0);
		
		l /= 2; m /= 2; n /= 2;
		
		return l + m + n >= q + r + s;
	}
	
	public static void main(String[] args) {
		new MagicSpheres();
	}
	
}