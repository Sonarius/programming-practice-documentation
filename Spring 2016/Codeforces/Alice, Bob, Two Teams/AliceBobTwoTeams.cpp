#include <bits/stdc++.h>
using namespace std;

const int MAX_INT = std::numeric_limits<int>::max();
const int MIN_INT = std::numeric_limits<int>::min();
const int INF = 1.0e9;
const int NEG_INF = -1.0e9;

#define MEM(arr,val)memset(arr,val, sizeof arr)
#define PI acos(0)*2.0
#define eps 1.0e-9
#define are_equal(a,b)fabs(a-b)<eps
#define LS(b)(b& (-b)) // Least significant bit
#define DEG_to_RAD(a)((a*PI)/180.0) // convert to radians

#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef pair<int,char> ic;
typedef pair<long,char> lc;
typedef vector<int> vi;
typedef vector<ii> vii;

int gcd(int a,int b){return b == 0 ? a : gcd(b,a%b);}
int lcm(int a,int b){return a*(b/gcd(a,b));}

int main() {
	FASTER;
	
	bool debugging = false;
	
	int i, n;
	ll m = 0, str = 0;
	cin >> n;
	int p[n];
	ll sum[2][n];
	string s;
	
	for (i = 0; i < n; i++) {
		cin >> p[i];
	}
	cin >> s;
	for (i = 0; i < n; i++) {
		if (s[i] == 'B') {
			str += p[i];
			p[i] *= -1;
		}
		if (debugging) printf("p[%d] = %d\n", i, p[i]);
	}
	
	sum[0][0] = p[0];
	sum[1][n - 1] = p[n - 1];
	
	for (int i = 1; i < n; i++) {
		sum[0][i] = p[i] + sum[0][i - 1];
	}
	
	for (int i = n - 2; i >= 0; i--) {
		sum[1][i] = p[i] + sum[1][i + 1];
	}
	
	for (int i = 0; i < n; i++) {
		if (debugging) printf("sum[%d][%d] = %d\t", 0, i, sum[0][i]);
		if (debugging) printf("sum[%d][%d] = %d\n", 0, i, sum[1][i]);
		
		m = max(m, sum[0][i]);
		m = max(m, sum[1][i]);
	}
	
	cout << (m + str) << endl;
	
	return 0;
}