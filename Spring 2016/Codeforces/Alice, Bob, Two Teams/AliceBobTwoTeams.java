import java.util.*;
import java.io.*;

public class AliceBobTwoTeams {
	
	boolean debugging = false;
	
	Scanner in;
	BufferedReader br;
	StringTokenizer st;
	StringBuilder sb;
	
	int n;
	long max, s;
	char[] set;
	long[][] sum; 
	
	public AliceBobTwoTeams() {
		in = new Scanner(System.in);
		
		n = in.nextInt();
		sum = new long[2][n];
		max = 0; s = 0;
		
		for (int i = 0; i < n; i++) {
			sum[0][i] = sum[1][i] = in.nextInt();
		}
		in.nextLine();
		
		set = in.nextLine().toCharArray();
		
		for (int i = 0; i < n; i++) {
			if (set[i] == 'B') {
				s += sum[0][i];
				sum[0][i] = sum[1][i] *= -1;
				
			}
			
		}
		
		if (debugging) {
			System.out.printf("\tValues: %s\n", Arrays.toString(sum[0]));
		}
		
		max = Math.max(max, sum[0][0]);
		max = Math.max(max, sum[1][n - 1]);
		for (int i = 1; i < n; i++) {
			sum[0][i] += sum[0][i - 1];
			sum[1][n - i - 1] += sum[1][n - i];
			
			if (debugging) {
				System.out.printf("\tSum[%d][%d] = %d\t", 0, i, sum[0][i]);
				System.out.printf("Sum[%d][%d] = %d\n", 1, (n - i - 1), sum[1][n - i]);
			}
			
			max = Math.max(max, sum[0][i]);
			max = Math.max(max, sum[1][n - i - 1]);
		}
		
		System.out.println(s + max);
		
		in.close();
	}
	
	public static void main(String[] args) {
		new AliceBobTwoTeams();
	}
	
}