#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define eps (1.0e-9)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,ll> ii;
typedef vector<ll> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

vector<vii> tree;
vi toSchool;
vi dist;

void dfs(int u, int p = -1){
//	printf("p,u= %d %d\n",p,u);
	for (int i = 0; i < tree[u].size(); ++i) {
		int v = tree[u][i].first;
		if(v != p){
			dist[v] = dist[u] + tree[u][i].second;
			dfs(v,u);
		}
	}

}

int main(){
	FASTER;


	int n;
	cin >> n;
	dist.assign(n+1,0);
	toSchool.assign(n+1,0);
	tree.assign(n+1,vii());

	for (int i = 0; i < n+1; ++i) {
		cin >> toSchool[i];
	}
	ll total = 0;
	for (int i = 0; i < n; ++i) {
		int u,v,w;
		cin >> u >> v >> w;
		total+=w;
		tree[u].push_back(ii(v,w));
		tree[v].push_back(ii(u,w));
//		printf("add edge %d %d\n", u,v);
	}
	dfs(0);
	ll ans = 1e18;
	for (int i = 0; i <= n; ++i) {
//		printf("dist[%d] = %lld\n", i, dist[i]);
		ll tmp = dist[i] + 2*(total-dist[i]) + toSchool[i];
		ans = min(ans , tmp);
	}

	cout << ans << endl;

	return 0;
}