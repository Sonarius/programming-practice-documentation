#include <bits/stdc++.h>
using namespace std;

const int MAX_INT = std::numeric_limits<int>::max();
const int MIN_INT = std::numeric_limits<int>::min();
const int INF = 1.0e9;
const int NEG_INF = -1.0e9;

#define max(a,b)(a>b?a:b)
#define min(a,b)(a<b?a:b)
#define MEM(arr,val)memset(arr,val, sizeof arr)
#define PI acos(0)*2.0
#define eps 1.0e-9
#define are_equal(a,b)fabs(a-b)<eps
#define LS(b)(b& (-b)) // Least significant bit
#define DEG_to_RAD(a)((a*PI)/180.0) // convert to radians

#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef pair<int,char> ic;
typedef pair<long,char> lc;
typedef vector<int> vi;
typedef vector<ii> vii;

int gcd(int a,int b){return b == 0 ? a : gcd(b,a%b);}
int lcm(int a,int b){return a*(b/gcd(a,b));}

int main() {
	FASTER;	
	
	bool debugging = false;
	
	int i, r, c;
	int width, height, x, y, k, n;
	string s;
	
	cin >> width >> height >> x >> y >> s;
	x--; y--; k = 0; n = s.size();
	
	if (debugging) {
		printf("\nWidth: %d\tHeight: %d\tX: %d\tY:%d\t\nS: %s\tlen(S): %d\n\n", width, height, x, y, s.c_str(), n);
	}
	
	int grid[width][height];
	int a[n + 1];
	MEM(a, 0);
	
	for (r = 0; r < height; r++) {
		for (c = 0; c < width; c++) {
			grid[c][r] = n;
		}
	}
	
	grid[x][y] = k++;
	
	if (debugging) {
			printf("\tCurrent (X,Y,K):\t (%d,%d,%d)\n", x, y, k);
	}
	
	for (i = 0; i < n; i++) {
		switch (s[i]) {
			case 'U':
			x = max(x - 1, 0);
			break;
			case 'D':
			x = min(x + 1, width - 1);
			break;
			case 'L':
			y = max(y - 1, 0);
			break;
			case 'R':
			y = min(y + 1, height - 1);
			break;
		}
		grid[x][y] = min(grid[x][y], k);
		k++;
		
		if (debugging) {
			printf("\tCurrent (X,Y,K):\t (%d,%d,%d)\n", x, y, k);
		}
	}
	
	if (debugging) {
		printf("\nGrid:\n");
	}
	
	for (r = 0; r < height; r++) {
		for (c = 0; c < width; c++) {
			if (debugging) {
				printf(" %4d", grid[c][r]);
			}
			
			a[grid[c][r]]++;
		}
		if (debugging) {
			printf("\n");
		}
	}
	
	printf("%d", a[0]);
	
	for (i = 1; i < n; i++) {
		printf(" %d", a[i]);
	}
	
	printf(" %d\n", a[n]);
	
	return 0;
}