import java.util.Scanner;
import java.util.Arrays;
import java.util.StringTokenizer;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class TestingRobots {
	
	boolean debugging = false;
	
	Scanner in;
	BufferedReader br;
	StringTokenizer st;
	StringBuilder sb;
	
	int width, height, x, y, k;
	char[] s;
	int[] a;
	int[][] grid;
	
	public TestingRobots() throws IOException {
		br = new BufferedReader(new InputStreamReader(System.in));
		st = new StringTokenizer(br.readLine());
		
		width = toInt(st.nextToken()); height = toInt(st.nextToken());
		x = toInt(st.nextToken()); y = toInt(st.nextToken());
		k = 0; x--; y--;
		s = br.readLine().toCharArray();
		
		a = new int[s.length + 1];
		grid = new int[width][height];
		
		for (int[] t: grid) {
			Arrays.fill(t, s.length);
		}
		
		grid[x][y] = k++;
		
		sb = new StringBuilder();
		
		if (debugging) {
			System.out.printf("\nWidth: %d\tHeight: %d\tX: %d\tY:%d\t\nS: %s\nA: %s\n\n",
			width, height, x, y, Arrays.toString(s), Arrays.toString(a));
		}
		
		for (char c: s) {
			doMove(c);
		}
		
		if (debugging) {
			printGrid();
		}
		
		for (int r = 0; r < height; r++) {
			for (int c = 0; c < width; c++) {
				a[grid[c][r]]++;
			}
		}
		
		sb.append("" + a[0]);
		
		for (int i = 1; i < a.length; i++) {
			sb.append(" " + a[i]);
		}
		
		System.out.println(sb.toString());
		
		br.close();
	}
	
	public int toInt(String s) {
		return Integer.parseInt(s);
	}
	
	public void printGrid() {
		System.out.println("Grid:");
		for (int r = 0; r < height; r++) {
			System.out.print("\t");
			for (int c = 0; c < width; c++) {
				System.out.print(grid[c][r] + " ");
			}
			System.out.print("\n");
		}
	}
	
	public boolean outOfBounds(int x1, int y1) {
		return x1 < 0 || x1 >= width || y1 < 0 || y1 >= height;
	}
	
	public int min(int a, int b) {
		return Math.min(a, b);
	}
	
	public void doMove(char c) {
		switch (c) {
			case 'U':
			if (!outOfBounds(x - 1, y)) x--;
			break;
			case 'R':
			if (!outOfBounds(x, y + 1)) y++;
			break;
			case 'L':
			if (!outOfBounds(x, y - 1)) y--;
			break;
			case 'D':
			if (!outOfBounds(x + 1,y)) x++;
			break;
		}
		
		grid[x][y] = min(grid[x][y], k++);
	}
	
	public static void main(String[] args) throws IOException {
		new TestingRobots();
	}
	
}