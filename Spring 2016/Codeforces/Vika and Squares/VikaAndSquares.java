import java.util.*;

public class VikaAndSquares {
	
	boolean debugging = false;
	
	int n;
	long min, max;
	long[] a;
	long ans;
	
	public VikaAndSquares() {
		Scanner in = new Scanner(System.in);
		
		n = in.nextInt();
		a = new long[n];
		min = Integer.MAX_VALUE;
		max = 0;
		
		for (int i = 0; i < n; i++) {
			a[i] = in.nextInt();
			min = Math.min(min, a[i]);
		}
		
		for (int i = 0; i < n; i++) {
			a[i] = a[i] == min ? 0 : 1;
		}
				
		for (int i = 1; i <= 2 * n; i++) {
			a[i % n] = a[i % n] == 0  ? 0 : a[(i - 1) % n] + 1;
			max = Math.max(max, a[i % n]);
		}
		
		ans = min * n + max;
		
		if (debugging) {
			System.out.printf("Min: %d\tN: %d\t Max: %d\n", min, n, max);
		}
		
		System.out.println(ans);
		
		in.close();
	}
	
	public static void main(String[] args) {
		new VikaAndSquares();
	}
	
}