
#include <iostream>
using namespace std;

#define max(a,b) (a>b?a:b)
#define min(a,b) (a<b?a:b)

typedef long long ll;

int main() {
	
	int n, i;
	cin >> n;
	long long min, max, ans;
	long long a[n];
	
	min = 9223372036854775807;
	max = 0;
	
	for (i = 0; i < n; i++) {
		cin >> a[i];
		min = min(min, a[i]);
	}
	
	for (i = 0; i < n; i++) {
		a[i] = a[i] == min ? 0 : 1;
	}
	
	for (i = 1; i <= 2 * n; i++) {
		a[i % n] = a[i % n] ? a[(i - 1) % n] + 1 : 0;
		max = max(max, a[i%n]);
	}
	
	ans = min * n + max;
	
	cout << ans << endl;
	
	return 0;
}
