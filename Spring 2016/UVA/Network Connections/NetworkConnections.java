import java.util.*;
import java.io.*;

public class NetworkConnections {
	
	boolean debugging = false;
	
	Scanner in;
	BufferedReader br;
	StringTokenizer st;
	StringBuilder sb;
	
	int[] ds;
	char com;
	int t, n, comp1, comp2, sa, ua;
	
	
	public NetworkConnections() throws IOException {
		br = new BufferedReader(new InputStreamReader(System.in));
		
		t = Integer.parseInt(br.readLine());
		br.readLine();
		
		while (t-- > 0) {
			n = Integer.parseInt(br.readLine());
			ds = new int[n + 1];
			Arrays.fill(ds, -1);
			
			if (debugging){
				System.out.println("Num of Comps: " + n);
			}
			
			sa = ua = 0;
			
			String line;
			while((line = br.readLine()) != null && !line.equals("")) {
				st = new StringTokenizer(line);
				com = st.nextToken().charAt(0);
				comp1 = Integer.parseInt(st.nextToken());
				comp2 = Integer.parseInt(st.nextToken());
				
				if (com == 'c') {
					if (debugging) {
						System.out.printf("\tJoining %d and %d...\n", comp1, comp2);
					}
					join(comp1, comp2);
				} else {
					if (debugging) {
						System.out.printf("\tChecking %d and %d... ", comp1, comp2);
					}
					boolean pass = share_parent(comp1, comp2);
					if (debugging) {
						System.out.printf("%s!\n", pass ? "Pass" : "Failed");
					}
					sa += pass ? 1 : 0;
					ua += pass ? 0 : 1;
				}
			}
			
			System.out.printf("%d,%d\n", sa, ua);
			
			if (t != 0) {
				System.out.println();
			}
		}
		
		br.close();
	}
	
	public int parent(int comp) {
		if (ds[comp] < 0) return comp;
		
		return (ds[comp] = parent(ds[comp]));
	}
	
	public boolean share_parent(int u, int v) {
		return parent(u) == parent(v);
	}
	
	public void join (int u, int v) {
		if (share_parent(u, v)) return;
		
		int parU = parent(u), parV = parent(v);
		
		if (ds[parU] < ds[parV]) {
			ds[parV] = parU;
		} else if (ds[parU] > ds[parV]) {
			ds[parU] = parV;
		} else {
			ds[parU] = parV;
			ds[parV]--;
		}
	}
	
	public static void main(String[] args) throws IOException {
		new NetworkConnections();
	}
	
}