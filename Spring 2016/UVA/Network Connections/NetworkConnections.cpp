#include <bits/stdc++.h>
using namespace std;

const int MAX_INT = std::numeric_limits<int>::max();
const int MIN_INT = std::numeric_limits<int>::min();
const int INF = 1.0e9;
const int NEG_INF = -1.0e9;

#define max(a,b)(a>b?a:b)
#define min(a,b)(a<b?a:b)
#define MEM(arr,val)memset(arr,val, sizeof arr)
#define PI acos(0)*2.0
#define eps 1.0e-9
#define are_equal(a,b)fabs(a-b)<eps
#define LS(b)(b& (-b)) // Least significant bit
#define DEG_to_RAD(a)((a*PI)/180.0) // convert to radians

#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef pair<int,char> ic;
typedef pair<long,char> lc;
typedef vector<int> vi;
typedef vector<ii> vii;

int gcd(int a,int b){return b == 0 ? a : gcd(b,a%b);}
int lcm(int a,int b){return a*(b/gcd(a,b));}

vi ds;

int find(int u) {
	if (ds[u] < 0)
		return u;
	
	return ds[u] = find(ds[u]);
}
bool same_set(int u, int v) {
	return find(u) == find(v);
}
void join(int u, int v) {
	if(same_set(u,v))return;

	int pu = find(u), pv = find(v);
	
	if (ds[pu] < ds[pv]) {
		ds[pv] = pu;
	} else if (ds[pu] > ds[pv]) {
		ds[pu] = pv;
	} else {
		ds[pv] = pu;
		ds[pu]--;
	}
}



int main() {
	FASTER;
	
	bool debugging = false;
	char c;
	int t, n, comp1, comp2, sa, ua;
	string query;
	
	cin >> t;
	
	while(t--) {
		cin >> n;
		ds.assign(n + 1, -1);
		sa = ua = 0;
		
		getline(cin, query);
		
		while(getline(cin, query)) {
			
			stringstream ss;
			ss.str(query);
			
			if (query == "") break;
			
			ss >> c >> comp1 >> comp2;
	
			if (debugging) {
				cout << "C: " << c << "; Comp1: " << comp1 << "; Comp2: " << comp2 << endl;
			}
			
			if (c == 'q') {
				(same_set(comp1, comp2) ? sa : ua)++;
			} else {
				join(comp1, comp2);
			}
		}
		
		cout << sa << "," << ua << endl;
		
		if (t) {
			cout << endl;
		}
	}
	
	return 0;
}