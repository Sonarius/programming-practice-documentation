#include <bits/stdc++.h>
using namespace std;

const int MAX_INT = std::numeric_limits<int>::max();
const int MIN_INT = std::numeric_limits<int>::min();
const int INF = 1.0e9;
const int NEG_INF = -1.0e9;

#define max(a,b)(a>b?a:b)
#define min(a,b)(a<b?a:b)
#define MEM(arr,val)memset(arr,val, sizeof arr)
#define PI acos(0)*2.0
#define eps 1.0e-9
#define are_equal(a,b)fabs(a-b)<eps
#define LS(b)(b& (-b)) // Least significant bit
#define DEG_to_RAD(a)((a*PI)/180.0) // convert to radians

#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef pair<int,char> ic;
typedef pair<long,char> lc;
typedef vector<int> vi;
typedef vector<ii> vii;

int gcd(int a,int b){return b == 0 ? a : gcd(b,a%b);}
int lcm(int a,int b){return a*(b/gcd(a,b));}

int main() {
	FASTER;
	
	bool debugging = false;
	
	int T, r, c;
	int wall[9][9];
	
	cin >> T;
	
	while (T--) {
		for (r = 0; r < 9; r += 2) {
			for (c = 0; c < (r + 1); c += 2) {
				cin >> wall[r][c];
				if (debugging) printf("Adding %d to %d,%d...\n", wall[r][c], r, c);
			}
		}
		
		// Init odds
		for (r = 0; r < 9 - 2; r += 2) {
			for (c = 0; c < (r + 1); c += 2) {
				wall[r + 2][c + 1] = (wall[r][c] - wall[r + 2][c] - wall[r + 2][c + 2]) / 2;
				if (debugging) printf("Setting %d,%d to %d...\n", r + 2, c + 1, wall[r + 2][c + 1]);
			}
		}		
		
		// Init evens
		for (r = 1; r < 9 - 1; r += 2) {
			for (c = 0; c < (r + 1); c++) {
				wall[r][c] = wall[r + 1][c] + wall[r + 1][c + 1];
				if (debugging) printf("Setting %d,%d to %d...\n", r, c, wall[r][c]);
			}
		}
		
		// Print all bricks
		for (r = 0; r < 9; r++) {
			for (c = 0; c < (r + 1); c++) {
				printf("%s%d", (c == 0 ? "" : " "), wall[r][c]);
			}
			printf("\n");
		}
		
		if (debugging) printf("\n");
	}
	
	return 0;
}