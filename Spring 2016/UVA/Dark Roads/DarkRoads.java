import java.util.*;
import java.io.*;

public class DarkRoads {
	
	boolean debugging = true;
	
	Scanner in;
	BufferedReader br;
	StringTokenizer st;
	StringBuilder sb;
	
	int j, r, cost, minCost;
	ArrayList<Edge> roads;
	UFDS ufds;
	
	public DarkRoads() {
		in = new Scanner(System.in);
		
		while ((j = in.nextInt()) + (r = in.nextInt()) != 0) {
			roads = new ArrayList<Edge>(r);
			ufds = new UFDS(j);
			cost = 0;
			for (int i = 0; i < r; i++) {
				roads.add(new Edge(in.nextInt(), in.nextInt(), in.nextInt()));
				cost += roads.get(i).weight;
			}
			
			Collections.sort(roads);
			
			minCost = 0;
			for (Edge e : roads) {
				if (!ufds.sameSet(e.u, e.v)) {
					minCost += e.weight;
					ufds.unionSet(e.u, e.v);
				}
			}
			
			System.out.println(cost - minCost);
		}
		
		in.close();
	}
	
	public static void main(String[] args) {
		new DarkRoads();
	}
	
}

class UFDS {
	
	int size;
	int[] rank, p;
	
	public UFDS(int size) {
		this.size = size;
		rank = new int[size];
		p = new int[size];
		for (int i = 0; i < size; i++) p[i] = i;
	}
	
	public int findSet(int i) {
		return p[i] == i ? i : (p[i] = findSet(p[i]));
	}
	
	public boolean sameSet(int i, int j) {
		return findSet(i) == findSet(j);
	}
	
	public void unionSet(int i, int j) {
		int x = findSet(i), y = findSet(j);
		if (x == y) return;
		
		if (rank[x] > rank[y])
			p[y] = x;
		else {
			p[x] = y;
			if (rank[x] == rank[y]) rank[y]++;
		}
	} 
}

class Edge implements Comparable<Edge> {
	
	int weight, u, v;
	
	public Edge(int u, int v, int weight) {
		this.u = u;
		this.v = v;
		this.weight = weight;
	}
	
	public int compareTo(Edge other) {
		return this.weight - other.weight;
	}
	
}