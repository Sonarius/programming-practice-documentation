This problem is describing the most common, practical application of an MST, finiding the least amount of money needed to connect a network. In this problem, I applied Kruskal's algorithm for generating an MST and implemented cycle-checking using UFDS.

The solution runs in O(NlogN) time. NlogN time for sorting the list of edges and C for checking if each edge is creates a cycle.