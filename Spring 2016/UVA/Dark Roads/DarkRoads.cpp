#include <bits/stdc++.h>
using namespace std;

const int MAX_INT = std::numeric_limits<int>::max();
const int MIN_INT = std::numeric_limits<int>::min();
const int INF = 1.0e9;
const int NEG_INF = -1.0e9;

#define MEM(arr,val)memset(arr,val, sizeof arr)
#define PI acos(0)*2.0
#define eps 1.0e-9
#define are_equal(a,b)fabs(a-b)<eps
#define LS(b)(b& (-b)) // Least significant bit
#define DEG_to_RAD(a)((a*PI)/180.0) // convert to radians

#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef pair<int,char> ic;
typedef pair<long,char> lc;
typedef pair<int,ii> iii;
typedef vector<int> vi;
typedef vector<ii> vii;
typedef vector<iii> viii;

int gcd(int a,int b){return b == 0 ? a : gcd(b,a%b);}
int lcm(int a,int b){return a*(b/gcd(a,b));}

class UnionFind {
	private:
		vi p, rank;
	public:
		UnionFind(int N) {
			rank.assign(N, 0);
			p.assign(N, 0);
			for (int i = 0; i < N; i++) p[i] = i;
		}
		int findSet(int i) {
			return (p[i] == i) ? i : (p[i] = findSet(p[i]));
		}
		bool isSameSet(int i, int j) {
			return findSet(i) == findSet(j);
		}
		void unionSet(int i, int j) {
			if (!isSameSet(i, j)) {
				int x = findSet(i), y = findSet(j);
				if (rank[x] > rank[y])
					p[y] = p[x];
				else if (rank[x] < rank[y])
					p[x] = p[y];
				else {
					p[x] = p[y];
					rank[y]++;
				}
			}
		}
};

int main() {
	FASTER;
	
	int m, n, u, v, w, tw;
	
	cin >> m >> n;
	while (m && n) {
		tw = 0;
		viii EdgeList;
		for (int i = 0; i < n; i++) {
			cin >> u >> v >> w;
			tw += w;
			EdgeList.push_back(make_pair(w, ii(u, v)));
		}
		sort(EdgeList.begin(), EdgeList.end());
		
		int mst_cost = 0;
		UnionFind UF(m);
		for (int i = 0; i < n; i++) {
			iii front = EdgeList[i];
			if (!UF.isSameSet(front.second.first, front.second.second)) {
				mst_cost += front.first;
				UF.unionSet(front.second.first, front.second.second);
			}
		}
		
		cout << (tw - mst_cost) << endl;
		cin >> m >> n;
	}
	
	return 0;
}