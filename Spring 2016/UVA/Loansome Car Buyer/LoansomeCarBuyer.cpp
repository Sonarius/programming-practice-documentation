#include <bits/stdc++.h>
using namespace std;

const int MAX_INT = std::numeric_limits<int>::max();
const int MIN_INT = std::numeric_limits<int>::min();
const int INF = 1.0e9;
const int NEG_INF = -1.0e9;

#define MEM(arr,val)memset(arr,val, sizeof arr)
#define PI acos(0)*2.0
#define eps 1.0e-9
#define are_equal(a,b)fabs(a-b)<eps
#define LS(b)(b& (-b)) // Least significant bit
#define DEG_to_RAD(a)((a*PI)/180.0) // convert to radians

#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef pair<int,char> ic;
typedef pair<long,char> lc;
typedef vector<int> vi;
typedef vector<ii> vii;

int gcd(int a,int b){return b == 0 ? a : gcd(b,a%b);}
int lcm(int a,int b){return a*(b/gcd(a,b));}

#define MAX_MONTHS 102

int main() {
	FASTER;
	
	bool debugging = false;
	
	int duration, records;
	double down_payment, loan, car_value, payment;
	double rate[MAX_MONTHS];
	cin >> duration;
	
	while (duration > 0) {
		cin >> down_payment >> loan >> records;
		
		car_value = loan + down_payment;
		payment = loan / duration;
		
		if (debugging) printf("New case with:\n\tDur: %d\tDownPay: %f\tLoan: %f\n\tRec: %d\tCarV: %f\tPay: %f\n", duration, down_payment, loan, records, car_value, payment);
		
		int month;
		double r;
		while (records--) {
			cin >> month >> r;
			if (debugging) printf("\tInitializing rate with (%d,%f)...\n", month, r);
			while (month < MAX_MONTHS) rate[month++] = r;
		}
		
		month = 0;
		car_value *= (1 - rate[month++]);
		while (car_value < loan) {
			car_value *= (1 - rate[month++]);
			loan -= payment;
		}
		
		month--;
		
		printf("%d month%s\n", month, (month == 1 ? "" : "s"));
		
		cin >> duration;
	}
	return 0;
}