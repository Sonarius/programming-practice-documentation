import java.util.Scanner;
import java.util.StringTokenizer;
import java.io.BufferedReader;

public class SnowmansHat {
	
	boolean debugging;
	
	Scanner in;
	BufferedReader br;
	StringTokenizer st;
	StringBuilder sb;
	
	int t;
	double R, H, r, h, a;
	
	public SnowmansHat() {
		in = new Scanner(System.in);
		
		t = in.nextInt();
		
		while (t-- > 0) {
			R = in.nextDouble();
			H = in.nextDouble();
			
			System.out.printf("%.2f\n", solve());
		}
		
		in.close();
	}
	
	public double solve() {
		return Math.sqrt(H*H+2*H*R)*R*(H*H+2*H*R)/(H+R)/(H+R);
	}
	
	public static void main(String[] args) {
		new SnowmansHat();
	}
	
}