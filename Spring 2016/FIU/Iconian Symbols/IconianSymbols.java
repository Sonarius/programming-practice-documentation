import java.util.*;
import java.io.*;

public class IconianSymbols {
	
	boolean debugging;
	
	Scanner in;
	BufferedReader br;
	StringTokenizer st;
	StringBuilder sb;
	
	int n;
	int opr;
	String opd1, opd2, res;
	
	public IconianSymbols() throws IOException {
		br = new BufferedReader(new InputStreamReader((System.in)));
		
		n = Integer.parseInt(br.readLine());
		
		while (n-- > 0) {
			st = new StringTokenizer(formatLine(br.readLine()));
			opd1 = st.nextToken();
			opd2 = st.nextToken();
			res = st.nextToken();
			
			System.out.println(solve());
		}
		
		br.close();
	}
	
	public int solve() {
		int i;
		for (i = 0; i < 10; i++) {
			sb = new StringBuilder(opd1);
			replace(sb, '?', (char)(i + '0'));
			int a = Integer.parseInt(sb.toString());
			
			sb = new StringBuilder(opd2);
			replace(sb, '?', (char)(i + '0'));
			int b = Integer.parseInt(sb.toString());
			
			sb = new StringBuilder(res);
			replace(sb, '?', (char)(i + '0'));
			int c = Integer.parseInt(sb.toString());
			
			if (check(a, b, c)) {
				return i;
			}
		}
		
		return -1;
	}
	
	public void replace(StringBuilder x, char c1, char c2) {
		int idx = 1;
		while ((idx = x.indexOf("" + c1)) != -1) {
			sb.setCharAt(idx, c2);
		}
	}
	
	public boolean check(int a, int b, int c) {
		switch (opr) {
			case 0:
				return a + b == c;
			case 1:
				return a * b == c;
			case 2:
				return a - b == c;
			default:
				return false;
		}
	}
	
	public String formatLine(String s) {
		sb = new StringBuilder(s);
		
		int idx = sb.indexOf("=");
		
		sb.setCharAt(idx, ' ');
		
		if ((idx = sb.indexOf("+")) != -1) {
			sb.setCharAt(idx, ' ');
			opr = 0;
			return sb.toString();
		}
		
		if ((idx = sb.indexOf("*")) != -1) {
			sb.setCharAt(idx, ' ');
			opr = 1;
			return sb.toString();
		}
		
		opr = 2;
		
		if ((idx = sb.indexOf("--")) != -1) {
			sb.setCharAt(idx, ' ');
			return sb.toString();
		}
		
		int temp = sb.indexOf(" ");
		idx = sb.lastIndexOf("-", temp);
		sb.setCharAt(idx, ' ');
		return sb.toString();
	}
	
	public static void main(String[] args) throws IOException {
		new IconianSymbols();
	}
	
}