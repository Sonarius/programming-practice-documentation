import java.util.*;
import java.io.*;

public class ExchangingLetters {
	
	boolean debugging = true;
	
	Scanner in;
	BufferedReader br;
	StringTokenizer st;
	StringBuilder sb;
	
	int t, n, m;
	int[] countOf;
	boolean[][] contains;
	char[][] input;
	
	public ExchangingLetters() {
		in = new Scanner(System.in);
		
		t = in.nextInt();
		
		while (t-- > 0) {
			n = in.nextInt();
			m = in.nextInt();
			in.nextLine();
			
			input = new char[n][m];
			contains = new boolean[m][26];
			countOf = new int[m];
			
			for (int i = 0; i < n; i++) {
				input[i] = in.nextLine().toCharArray();
				
				for (int j = 0; j < m; j++) {
					char c = input[i][j];
					if (!contains[j][c - 'A']) {
						contains[j][c - 'A'] = true;
						countOf[j]++;
					}
				}
			}
			
			long res = 1;
			
			for (int count : countOf) {
				res = (res % 1000000007) * count;
			}
			
			System.out.println(res);
		}
		
		in.close();
	}
	
	public void printInput() {
		for (int i = 0; i < n; i++) {
			System.out.println(Arrays.toString(input[i]));
		}
	}
	
	public static void main(String[] args) {
		new ExchangingLetters();
	}
	
}