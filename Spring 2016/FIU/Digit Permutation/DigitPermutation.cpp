#include <bits/stdc++.h>
using namespace std;

const int MAX_INT = std::numeric_limits<int>::max();
const int MIN_INT = std::numeric_limits<int>::min();
const int INF = 1.0e9;
const int NEG_INF = -1.0e9;

#define max(a,b)(a>b?a:b)
#define min(a,b)(a<b?a:b)
#define MEM(arr,val)memset(arr,val, sizeof arr)
#define PI acos(0)*2.0
#define eps 1.0e-9
#define are_equal(a,b)fabs(a-b)<eps
#define LS(b)(b& (-b)) // Least significant bit
#define DEG_to_RAD(a)((a*PI)/180.0) // convert to radians

#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef pair<int,char> ic;
typedef pair<long,char> lc;
typedef vector<int> vi;
typedef vector<ii> vii;

int gcd(int a,int b){return b == 0 ? a : gcd(b,a%b);}
int lcm(int a,int b){return a*(b/gcd(a,b));}

ll fact[20] =  {1ll, 1ll, 2ll, 6ll, 24ll, 120ll, 720ll, 5040ll, 40320ll, 362880ll, 3628800ll, 39916800ll, 479001600ll, 6227020800ll, 87178291200ll, 1307674368000ll, 20922789888000ll, 355687428096000ll, 6402373705728000ll, 121645100408832000ll};

int main() {
	FASTER;
	
	bool debugging = false;
	
	int t = 0, sum = 0;
	int num[16];
	string n;
	
	cin >> t;
	
	while (t--) {
		cin >> sum;
		
		cin >> n;
		
		MEM(num, 0);
		
		if (debugging) {
			printf("\tSum: %d\n", sum);
		}
		
		// Init num
		int i;
		for (i = 0; i < sum; i++) {
			if (n[i] <= '9') {
				num[n[i] - '0']++;
			} else {
				num[n[i] - 'A' + 10]++;
			}
			
			if (debugging) {
				printf("\t\tN[%d]: %c\n", i, n[i]);
			}
			
		}
		
		ll a = fact[sum];
		
		if (debugging) {
			printf("\t\tA.1: %ld (Fact[sum]: %ld)\n", a, fact[sum]);
		}
		
		a -= (num[0] * fact[sum - 1]);
		
		if (debugging) {
			printf("\t\tA.2: %ld (Fact[sum - 1]: %ld)\n", a, fact[sum - 1]);
		}
		
		for (i = 0; i < 16; i++) {
			a /= fact[num[i]];
			if (debugging) {
				printf("\t\tA.3.%d: %ld (A / Fact[num[%d]]: %ld / %ld)\n", i, a, a, i, fact[num[i]]);
			}
		}
		
		cout << a << endl;
	}
	
	
	return 0;
}