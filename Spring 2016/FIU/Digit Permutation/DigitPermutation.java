import java.util.*;
import java.io.*;

public class DigitPermutation {
	
	boolean debugging = false;
	
	Scanner in;
	BufferedReader br;
	StringTokenizer st;
	StringBuilder sb;
	
	int t;
	int sum;
	char[] n;
	int[] num;
	
	long[] fact =  {1l, 1l, 2l, 6l, 24l, 120l, 720l, 5040l, 40320l, 362880l, 3628800l, 39916800l, 479001600l, 6227020800l, 87178291200l, 1307674368000l, 20922789888000l, 355687428096000l, 6402373705728000l, 121645100408832000l};
	
	public DigitPermutation() throws IOException{
		in = new Scanner(System.in);
		
		t = in.nextInt();
		
		while (t-- > 0) {
			sum = in.nextInt();
			n = in.next().toCharArray();
			
			if (debugging) {
				System.out.printf("N: %s\n", Arrays.toString(n));
			}
			
			initNum();
			
			if (debugging) {
				System.out.printf("Sum:%d\nNum: %s\n", sum, Arrays.toString(num));
			}
			
			System.out.println(solve());
		}
		
		in.close();
	}
	
	public long solve() {
		long a = fact[sum];
		
		if (debugging) {
			System.out.printf("\tStep 1: a = %d\n", a);
		}
		
		a -= num[0] * fact[sum - 1];
		
		if (debugging) {
			System.out.printf("\tStep 2: a = %d\n", a);
		}
		
		for (int i = 0; i < num.length; i++) {
			int d = num[i];
			a /= fact[d];
			
			if (debugging) {
				System.out.printf("\tStep 3.%d: a = %d\n", i, a);
			}
		}
		
		return a;
	}
	
	public void initNum() {
		num = new int[16];
		
		for (char c : n) {
			if (debugging) {
				System.out.printf("Incrementing %c (%d)...\n", c, c < '9' ? c - '0' : c + 10 - 'A');
			}
			
			if (c <= '9') {
				num[c - '0']++;
			} else {
				num[c - 'A' + 10]++;
			}
		}
	}
	
	public static void main(String[] args) throws IOException {
		new DigitPermutation();
	}
	
}