package Medium;

import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class APUInitPhase {
	
	static boolean debugging = true;
	
	static int width, height;
	static Tile[][] map;
	
	public static void main(String[] args) throws IOException {
		Scanner in = new Scanner(System.in);
		
		width = in.nextInt();
		height = in.nextInt();
		in.nextLine();
		
		map = new Tile[height][width];
		
		if (debugging) {
			System.err.println("Board:");
		}
		
		for (int row = 0; row < height; row++) {
			char[] tiles = in.nextLine().toCharArray();
			
			if (debugging) {
				System.err.println(Arrays.toString(tiles));
			}
			
			for (int col = 0; col < width; col++) {
				if (tiles[col] == '0') {
					map[row][col] = new Tile();
				}
			}
		}
		
		for (int row = 0; row < height; row++) {
			for (int col = 0; col < width; col++) {
				if (map[row][col] != null) {
					
					if (debugging) {
						System.err.println("Processing: (" + col + ", " + row + ") ...");
					}
					
					print(row, col);
					System.out.println();
				}
			}
		}		
		
		in.close();
	}
	
	public static void print(int row, int col) {
		System.out.print(col + " " + row + " ");
		
		int coli = col;
		
		while (++col < width && map[row][col] == null);
		
		if (col >= width || map[row][col] == null) {
			System.out.print("-1 -1 ");
		} else {
			System.out.print(col + " " + row + " ");
		}
		
		col = coli;
		
		while (++row < height && map[row][col] == null);
		
		if (row >= height || map[row][col] == null) {
			System.out.print("-1 -1");
		} else {
			System.out.print(col + " " + row);
		}
	}
	
}

class Tile {
	
	boolean scanned;
	
	public Tile() {
		scanned = false;
	}
	
	public void scan() {
		scanned = true;
	}
}
