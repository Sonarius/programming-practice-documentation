package Medium;

import java.io.IOException;
import java.util.Scanner;

public class HeatDetector {		
	
	static Scanner in;
	
	static int height, width, n, minX, minY, midX, midY, maxX, maxY;
	
	public static void main(String[] args) throws IOException {
		in = new Scanner(System.in);
		
		width = in.nextInt();
		height = in.nextInt();
		
		n = in.nextInt();
		
		midX = in.nextInt();
		midY = in.nextInt();
		
		minX = 0;
		minY = 0;
		maxX = width - 1;
		maxY = height - 1;
		
		in.nextLine();
		
		while (n-- > 0) {
			String signal = in.nextLine();
			boolean up = signal.indexOf('U') != -1, down = signal.indexOf('D') != -1, left = signal.indexOf('L') != -1, right = signal.indexOf('R') != -1;
			
			if (up) {
				maxY = midY - 1;
			}
			
			if (down) {
				minY = midY + 1;
			}
			
			midY = minY + ((maxY - minY) / 2);
			
			if (left) {
				maxX = midX - 1;
			}
			
			if (right) {
				minX = midX + 1;
			}
			
			midX = minX + ((maxX - minX) / 2);
			
			System.out.println(midX + " " + midY);
		}
		
		in.close();
	}
	
}
