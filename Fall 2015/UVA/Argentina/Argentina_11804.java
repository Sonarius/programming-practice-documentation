package contestvolumes.vol118;

import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class Argentina_11804 {
	
	static boolean debugging = false;
	static Scanner in;
	
	static int count = 0, cases;
	static Player[] players = new Player[10], attackers = new Player[5], defenders = new Player[5];
	
	public static void main(String[] args) throws IOException {
		in = new Scanner(System.in);
		
		cases = in.nextInt();
		in.nextLine();
		
		while (++count <= cases) {
			
			for (int i = 0; i < 10; i++) {
				players[i] = new Player(in.next(), in.nextInt(), in.nextInt());
			}
			
			Arrays.sort(players);
			
			if (debugging) {
				System.out.println(Arrays.toString(players));
			}
			
			for (int i = 0; i < 5; i++) {
				attackers[i] = players[i];
				attackers[i].comparingByName = true;
			}
			
			for (int i = 5; i < 10; i++) {
				defenders[i - 5] = players[i];
				defenders[i - 5].comparingByName = true;
			}
			
			Arrays.sort(attackers);
			Arrays.sort(defenders);
			
			System.out.println("Case " + count + ":");
			
			StringBuilder output = new StringBuilder();
			
			output.append(Arrays.toString(attackers));
			output.setCharAt(0, '(');
			output.setCharAt(output.length() - 1, ')');
			System.out.println(output);
			
			output = new StringBuilder();
			
			output.append(Arrays.toString(defenders));
			output.setCharAt(0, '(');
			output.setCharAt(output.length() - 1, ')');
			System.out.println(output);
		}
		
		in.close();
	}
	
}

class Player implements Comparable<Player> {
	
	boolean comparingByName = false;
	String name;
	int atk, def;
	
	public Player(String name, int atk, int def) {
		this.name = name;
		this.atk = atk;
		this.def = def;
	}

	@Override
	public int compareTo(Player other) {
		if (!comparingByName) {
			if (this.atk != other.atk) {
				return other.atk - this.atk;
			} else if (this.def != other.def) {
				return this.def - other.def;
			} else {
				return this.name.compareTo(other.name);
			}
		} else {
			return this.name.compareTo(other.name);
		}
	}
	
	@Override
	public String toString() {
		return this.name;
	}
	
}
