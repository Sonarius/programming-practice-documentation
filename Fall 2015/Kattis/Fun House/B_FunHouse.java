package Contests.icpc.na.reg2015.warmup3;
import java.io.IOException;
import java.util.Scanner;

public class B_FunHouse {
	
	static boolean debugging = false;
	static Scanner in;
	
	static int currCase = 1, width, height, row, col, direction = 0;
	static final int NORTH = 0, SOUTH = 2, EAST = 1, WEST = 3;
	
	static char[][] house;
	
	public static void main(String[] args) throws IOException {
		in = new Scanner(System.in);
		
		while (readInput()) {
			
			if (debugging) {
				System.out.println("\n");
			}
			
			while (house[row][col] != 'x') {
				move();
				
				if (debugging) {
					System.out.printf("Row: %d\tCol: %d\tCurr Char: %c\n", row, col, house[row][col]);
				}
			}
			
			house[row][col] = '&';
			
			
			System.out.println("HOUSE " + currCase++);
			printHouse();			
			
			if (debugging) {
				System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
			}
		}
		
		in.close();
	}
	
	public static void move() {
		switch (direction) {
		case NORTH:
			row--;
			
			direction = house[row][col] == '\\' ? WEST : direction;
			direction = house[row][col] == '/' ? EAST : direction;
			break;
		case SOUTH:
			row++;
			
			direction = house[row][col] == '\\' ? EAST : direction;
			direction = house[row][col] == '/' ? WEST : direction;
			break;
		case EAST:
			col++;
			
			direction = house[row][col] == '\\' ? SOUTH : direction;
			direction = house[row][col] == '/' ? NORTH : direction;
			break;
		case WEST:
			col--;
			
			direction = house[row][col] == '\\' ? NORTH : direction;
			direction = house[row][col] == '/' ? SOUTH : direction;
			break;
		}
	}
	
	public static void printHouse() {
		
		if (debugging) {
			System.out.println("Current house layout:");
		}
		
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				System.out.print(house[i][j]);
			}
			
			if (i != height) {
				System.out.println();
			}
		}
	}
	
	public static boolean readInput() {
		width  = in.nextInt();
		height = in.nextInt();
		
		if (width == 0 && height == 0) {
			return false;
		}
		
		in.nextLine();
		
		house = new char[height][width];
		
		for (int i = 0; i < height; i++) {
			char[] currLine = in.nextLine().toCharArray();
			
			for (int j = 0; j < width; j++) {
				house[i][j] = currLine[j];
				
				if (currLine[j] == '*') {
					row = i;
					col = j;
					
					if (row == 0) {
						direction = SOUTH;
					} else if (row == height - 1) {
						direction = NORTH;
					} else if (col == 0) {
						direction = EAST;
					} else {
						direction = WEST;
					}
				}
			}
					
		}
		
		if (debugging) {
			printHouse();
		}
		
		return true;
	}
	
}
