import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class I_SimonSays {
	
	static BufferedReader in;
	static StringTokenizer tokens;
	
	static int n;
	
	public static void main(String[] args) throws IOException {
		in = new BufferedReader(new InputStreamReader(System.in));
		
		tokens = new StringTokenizer(in.readLine());
		
		n = Integer.parseInt(tokens.nextToken());
		
		for (int i = 0; i < n; i++) {
			tokens = new StringTokenizer(in.readLine());
			
			if (tokens.nextToken().equals("Simon") && tokens.nextToken().equals("says")) {
				StringBuilder toPrint = new StringBuilder();
				
				while (tokens.hasMoreTokens()) {
					toPrint.append(tokens.nextToken() + " ");
				}
				
				System.out.println(toPrint.toString().substring(0, toPrint.length() - 1));
				
			} else {
				continue;
			}
		}
		
		in.close();
	}
	
	
	
}