package Contests.icpc.na.reg2015.warmup4;
import java.io.IOException;
import java.util.Scanner;

public class G_GrowlingGears {
	
	static Scanner in;
	
	static int cases, gears, a, b, c, currBestIdx;
	static double currBest;
	
	public static void main(String[] args) throws IOException {
		in = new Scanner(System.in);
		
		cases = in.nextInt();
		
		while (cases-- > 0) {
			currBest = 0.0; currBestIdx = 0;
			gears = in.nextInt();
			
			for (int i = 1; i <= gears; i++) {
				a = in.nextInt(); b = in.nextInt(); c = in.nextInt();
				
				double curr = calcMax();
				
				if (curr > currBest) {
					currBest = curr;
					currBestIdx = i;
				}
			}
			
			System.out.println(currBestIdx);
		}
		
		in.close();
	}
	
	public static double calcMax() {
		double x = b / 2.0 / a;
		
		return (-a * x * x) + (b * x) + c;
	}
	
}
