package Contests.icpc.na.reg2015.warmup5;

import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class D_HeightOrdering {
	
	static boolean debugging = false;
	static Scanner in;
	
	static int cases, caseNumber, toAdd, minIdx, toReturn;
	static int[] heights;
	
	public static void main(String[] args) throws IOException {
		in = new Scanner(System.in);
		
		cases = in.nextInt();
		caseNumber = 1;
		
		while (caseNumber <= cases) {
			if (debugging) {
				System.out.println("\n~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
			}
			
			in.nextInt();
			heights = new int[20];
			minIdx = 19;
			toReturn = 0;
			
			while (minIdx >= 0) {
				toAdd = in.nextInt();
				int idx = idxToIns(toAdd);
				toReturn += (19 - idx);
				
				if (debugging) {
					System.out.printf("%d should be placed at index %d\n", toAdd, idx);
					System.out.printf("%d people were taller than %d\n", (19 - idx), toAdd);
					System.out.println("Current heights: " + Arrays.toString(heights));
				}
				
				heights[minIdx] = toAdd;
				Arrays.sort(heights);
				minIdx--;
				
				if (debugging) {
					System.out.println("New heights: " + Arrays.toString(heights));
				}
			}
			
			System.out.println(caseNumber++ + " " + toReturn);
		}
		
		in.close();
	}
	
	public static int idxToIns(int n) {
		for (int i = 19; i >= minIdx; i--) {
			if (heights[i] <= n) {
				return i;
			}
		}
		
		return minIdx;
	}
	
}
