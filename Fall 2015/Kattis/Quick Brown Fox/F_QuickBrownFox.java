import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class F_QuickBrownFox {
	
	static BufferedReader in;
	
	static int n;
	static boolean[] contains;
	static char[] input;
	static StringBuilder output;
	
	public static void main(String[] args) throws IOException {
		in = new BufferedReader(new InputStreamReader(System.in));
		
		n = Integer.parseInt(in.readLine());
		
		while (n-- > 0) {
			contains = new boolean[26];
			input = in.readLine().toLowerCase().toCharArray();
			
			for (char c: input) {
				if (c >= 'a' && c <= 'z') {
					contains[(int) (c - 'a')] = true;
				}
			}
			
			output = new StringBuilder("missing ");
			
			for (int i = 0; i < contains.length; i++) {
				if (!contains[i]) {
					output.append((char) (i + 'a'));
				}
			}
			
			if (output.length() == 8) {
				System.out.println("pangram");
			} else {
				System.out.println(output);
			}
		}
		
		in.close();
	}
	
}