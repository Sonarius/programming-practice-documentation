import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.PriorityQueue;
import java.util.Scanner;
import java.util.StringTokenizer;

public class J_TornToPieces {
	
	static boolean debugging = false;
	static Scanner in;
	
	static int n;
	static String start, goal;
	static StringTokenizer query;
	static Graph graph;
	
	public static void main(String[] args) throws IOException {
		in = new Scanner(System.in);
		
		n = in.nextInt();
		in.nextLine();
		
		graph = new Graph();
		
		for (int i = 0; i < n; i++) {
			query = new StringTokenizer(in.nextLine());
			
			String connect1 = query.nextToken();
			
			if (!graph.nodes.containsKey(connect1)) {
				graph.nodes.put(connect1, new Node(connect1));
			}
			
			while (query.hasMoreTokens()) {				
				String connect2 = query.nextToken();
				
				if (debugging) {
					System.out.println("Connecting " + connect1 + " and " + connect2);
				}
				
				if (!graph.nodes.containsKey(connect2)) {
					graph.nodes.put(connect2, new Node(connect2));
				}
				
				graph.nodes.get(connect1).adj.add(graph.nodes.get(connect2));
				graph.nodes.get(connect2).adj.add(graph.nodes.get(connect1));
			}
		}
		
		start = in.next();
		goal = in.next();
		
		graph.initDijkstra(start);
		graph.initPath(goal);
		
		if (graph.nodes.get(goal).minCost == Integer.MAX_VALUE) {
			System.out.println("no route found");
		} else {
			for (int i = 0; i < graph.path.size(); i++) {
				System.out.print(graph.path.get(i));
				
				if (i != graph.path.size() - 1) {
					System.out.print(" ");
				}
			}
			
			System.out.println();
		}
		
		in.close();
	}
	
}

class Node implements Comparable<Node> {
	
	String id;
	int minCost;
	ArrayList<Node> adj;
	Node prev;
	
	public Node(String id) {
		this.id = id;
		minCost = Integer.MAX_VALUE;
		prev = null;
		adj = new ArrayList<Node>(32);
	}

	@Override
	public int compareTo(Node other) {
		return Integer.compare(this.minCost, other.minCost);
	}
	
	@Override
	public String toString() {
		return id;
	}
	
}

class Graph {
	
	HashMap<String, Node> nodes;
	ArrayList<Node> path;
	PriorityQueue<Node> queue;
	
	public Graph() {
		nodes = new HashMap<String, Node>(32);
	}
	
	public void initDijkstra(String start) {
		queue = new PriorityQueue<Node>(32);
		
		if (!nodes.containsKey(start)) {
			nodes.put(start, new Node(start));
		}
		
		nodes.get(start).minCost = 0;
		
		queue.add(nodes.get(start));
		
		while (!queue.isEmpty()) {
			Node current = queue.poll();
			ArrayList<Node> adjacent = current.adj;
			
			for (Node other: adjacent) {
				if (other.minCost > current.minCost + 1) {
					other.minCost = (current.minCost + 1);
					other.prev = current;
					queue.add(other);
				}
			}
		}
		
	}
	
	public void initPath(String goal) {
		path = new ArrayList<Node>(32);
		
		if (!nodes.containsKey(goal)) {
			nodes.put(goal, new Node(goal));
		}
		
		Node current = nodes.get(goal);
		
		while (current != null) {
			path.add(current);
			current = current.prev;
		}
		
		Collections.sort(path);
	}
	
}