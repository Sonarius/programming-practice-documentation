package Contests.icpc.na.reg2015.warmup5;

import java.io.IOException;
import java.util.Arrays;
import java.util.PriorityQueue;
import java.util.Scanner;

public class E_IslandsInTheDataStream {
	
	static boolean debugging = false;
	static Scanner in;
	
	static boolean open = false;
	static int cases, caseNumber = 0, toReturn;
	static int[] heights;
	static PriorityQueue<Integer> queue;
	
	public static void main(String[] args) throws IOException {
		in = new Scanner(System.in);
		
		cases = in.nextInt();
		
		while (++caseNumber <= cases) {
			in.nextInt();
			toReturn = 0;
			
			if (debugging) {
				System.out.println("\n~~~~~~~~~~~~~~~~~~~~\n");
			}
			
			heights = new int[12];
			queue = new PriorityQueue<Integer>(12);
			
			for (int i = 0; i < 12; i++) {
				heights[i] = in.nextInt();
				if (heights[i] != 0 && !queue.contains(-heights[i])) {
					queue.add(-heights[i]);
				}
			}
			
			if (debugging) {
				System.out.println("Heights: " + Arrays.toString(heights));
				System.out.println("Queue: " + queue.toString());
			}
			
			while (!queue.isEmpty()) {
				int height = -queue.poll();
				open = false;
				
				if (debugging) {
					System.out.printf("Processing %d\n", height);
				}
				
				for (int i = 1; i < 12; i++) {
					if (heights[i] == height) {
						heights[i] = queue.isEmpty() ? 0 : heights[i - 1];
						open = true;
					} else if (open) {
						open = false;
						toReturn++;
					}
				}
				
				if (debugging) {
					System.out.printf("%d islands counted so far...\n", toReturn);
					System.out.println("Heights: " + Arrays.toString(heights));
				}
			}
			
			System.out.println(caseNumber + " " + toReturn);
		}
		
		in.close();
	}
	
}
