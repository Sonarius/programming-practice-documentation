
import java.io.IOException;
import java.util.Scanner;

public class YikesBikes {
	
	static boolean debugging = false;
	static Scanner in;
	
	static boolean leftCrash, centerCrash, rightCrash;
	static int cases, numCases, bikeLeft, bikeCenter, bikeRight;
	static double maxSpeed, bikeSpeed, bikeOffset, maxStartTime, maxDelta, maxAngle, maxCenter, maxLeft, maxRight;
	
	public static void main(String[] args) throws IOException {
		in = new Scanner(System.in);
		
		cases = in.nextInt();
		numCases = cases;
		
		while (cases-- > 0) {
			maxSpeed = in.nextDouble();
			bikeSpeed = in.nextDouble();
			bikeOffset = in.nextDouble();
			maxStartTime = in.nextDouble();
			
			bikeOffset -= bikeSpeed * maxStartTime;
			
			if (debugging) {
				System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
				System.out.println("Current case: " + (numCases - cases));
				System.out.printf("Max Speed: %f; Bike Speed: %f\n", maxSpeed, bikeSpeed);
				System.out.printf("Lead Bike Position: %f\n", bikeOffset);
			}
			
			maxAngle = Math.atan2(bikeSpeed, maxSpeed);
			maxDelta = 1.0 / Math.cos(maxAngle);
			
			maxCenter = (5.5 * (bikeSpeed / maxSpeed));
			maxRight = maxCenter - maxDelta * 0.5;
			maxLeft = maxCenter + maxDelta * 0.5;
			
			if (debugging) {
				System.out.printf("Max Velocity Angle: %.5f\n", Math.toDegrees(maxAngle));
				System.out.printf("Left Side: %.5f;\tCenter: %.5f;\tRight Side: %.5f\n", maxLeft, maxCenter, maxRight);
				System.out.printf("Left Dist: %.5f;\tCenter Dist: %.5f\tRight Dist: %.5f\n", (maxLeft - bikeOffset), (maxCenter - bikeOffset), (maxRight - bikeOffset));
			}
			
			System.out.println(processCrashes());
		}
		
		in.close();
	}
	
	public static String processCrashes() {		
		bikeLeft = ((int) Math.floor(maxLeft - bikeOffset)) / 4 + 1;
		bikeCenter = ((int) Math.floor(maxCenter - bikeOffset)) / 4 + 1;
		bikeRight = ((int) Math.floor(maxRight - bikeOffset)) / 4 + 1;
		
		bikeLeft = Math.min(10, bikeLeft);
		bikeCenter = Math.min(10, bikeCenter);
		bikeRight = Math.min(10, bikeRight);
		
		int temp = ((int) Math.floor(maxLeft - bikeOffset + 128.0));
		leftCrash = temp % 4 < 2;
		temp = ((int) Math.floor(maxCenter - bikeOffset + 128.0));
		centerCrash = temp % 4 < 2 || (maxCenter - maxRight) > 2.0;
		temp = ((int) Math.floor(maxRight - bikeOffset + 128.0));
		rightCrash = temp % 4 < 2;
		
		if (debugging) {
			System.out.printf("Bike to the left: %d\tBike center: %d\t\tBike to the right: %d\n", bikeLeft, bikeCenter, bikeRight);
			System.out.println("Left crashes: " + leftCrash + "\tCenter crashes: " + centerCrash + "\tRight crashes: " + rightCrash);
		}
		
		if (maxLeft < bikeOffset) {
			return "Max beats the first bicycle";
		}
		
		if (maxRight > bikeOffset + 38) {
			return "Max crosses safely after bicycle 10";
		}
		
		if (leftCrash && rightCrash && centerCrash) {
			return "Collision with bicycle " + bikeRight;
		}
		
		if (centerCrash) {
			return "Collision with bicycle " + bikeCenter;
		}
		
		if (rightCrash) {
			return "Collision with bicycle " + bikeRight;
		}
		
		if (leftCrash) {
			return "Collision with bicycle " + bikeLeft;
		}
		
		return "Max crosses safely after bicycle " + bikeCenter;
	}	
}
