package Contests.icpc.na.reg2015.warmup6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class F_Timebomb {
	
	static boolean debugging = false;
	static BufferedReader in;
	static StringBuilder sb;
	
	static int x;
	static String digits;
	static String[] input;
	static Map<String, Integer> key;
	
	public static void main(String[] args) throws IOException {
		in = new BufferedReader(new InputStreamReader(System.in));
		key = new HashMap<String, Integer>(10);
		
		key.put("**** ** ** ****", 0);
		key.put("  *  *  *  *  *", 1);
		key.put("***  *****  ***", 2);
		key.put("***  ****  ****", 3);
		key.put("* ** ****  *  *", 4);
		key.put("****  ***  ****", 5);
		key.put("****  **** ****", 6);
		key.put("***  *  *  *  *", 7);
		key.put("**** ***** ****", 8);
		key.put("**** ****  ****", 9);
		
		sb = new StringBuilder(in.readLine());
		input = new String[(sb.length() + 1) / 4];
		digits = "";
		
		Arrays.fill(input, new String());

		for (int i = 0; i < input.length; i++) {
			input[i] += sb.substring(i * 4, i * 4 + 3);
			
			if (debugging) {
				System.out.printf("Added '%s' to input[%d]\n", sb.substring(i * 4, i * 4 + 3), i);
				System.out.printf("Value at input[%d]: %s\n", i, input[i]);
			}
		}
		
		for (int i = 1; i < 5; i++) {
			sb = new StringBuilder(in.readLine());
			for (int j = 0; j < input.length; j++) {
				input[j] += sb.substring(j * 4, j * 4 + 3);
				
				if (debugging) {
					System.out.printf("Added '%s' to input[%d]\n", sb.substring(j * 4, j * 4 + 3), j);
					System.out.printf("Value at input[%d]: %s\n", j, input[j]);
				}
			}
		}
		
		for (int i = 0; i < input.length; i++) {
			String conv = input[i];
			if (key.containsKey(conv)) {
				digits += key.get(conv);
			} else {
				if (debugging) {
					System.out.printf("Key did not contain the value: %s (length %d, idx %d)\n", conv, conv.length(), i);
				}
				digits = "" + -1;
				break;
			}
		}
		
		x = Integer.parseInt(digits);
		
		if (debugging) {
			System.out.printf("Value decoded from input: %d\n", x);
		}
		
		System.out.println(x == -1 || x % 6 != 0 ? "BOOM!!" : "BEER!!");
		
		in.close();
	}
	
}
