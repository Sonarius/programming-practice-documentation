package Contests.icpc.na.reg2015.warmup3;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class E_WordCloud {
	
	static boolean debugging = false;
	static Scanner in;
	
	static int currCase = 1, n, maxWidth, width, height, maxFreq;
	static Datum[] data;
	static Cloud cloud;
	
	public static void main(String[] args) throws IOException {
		in = new Scanner(System.in);
		
		while ((maxWidth = in.nextInt()) != 0 && (n = in.nextInt()) != 0) {
			in.nextLine();
			initData();
			//Arrays.sort(data);
			width = 0;
			height = 0;
			
			cloud = new Cloud(maxWidth, debugging);
			
			if (debugging) {
				System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
				System.out.println("Data: " + Arrays.toString(data));
				System.out.printf("Max frequency: %d\n", maxFreq);
			}
			
			for (Datum d : data) {
				cloud.add(d);
			}
			
			cloud.calcSize();
			
			width = cloud.width;
			height = cloud.height;
			
			System.out.printf("CLOUD %d: %d\n", currCase++, height);
			
		}
		
		in.close();
	}
	
	public static void initData() {
		ArrayList<Datum> temp = new ArrayList<Datum>(n);
		
		maxFreq = 0;
		
		for (int i = 0; i < n; i++) {
			Datum toAdd = new Datum(in.next(), in.nextInt());
			
			maxFreq = Math.max(maxFreq, toAdd.frequency);
			
			if (toAdd.frequency > 4) {
				temp.add(toAdd);
			}
		}
		
		data = new Datum[temp.size()];
		
		temp.toArray(data);
		
		for (Datum d : data) {
			d.initSize(maxFreq);
		}
	}
	
}

class Row {
	
	boolean debugging;
	
	int maxWidth = 0, width = 0, height = 0;
	ArrayList<Datum> container;
	
	public Row (int maxWidth, boolean d) {
		this.debugging = d;
		
		container = new ArrayList<Datum>(10);
		this.maxWidth = maxWidth;
	}
	
	public boolean add(Datum toAdd) {
		if (container.isEmpty()) {
			width = toAdd.width;
			height = toAdd.height;
			container.add(toAdd);
			return true;
		}
		
		if (toAdd.width + width + 10  > maxWidth) {
			return false;
		}
		
		container.add(toAdd);
		width += toAdd.width + 10;
		height = Math.max(height, toAdd.height);
		
		return true;
	}
	
}

class Cloud {
	
	boolean debugging;
	
	int maxWidth = 0, width = 0, height = 0;
	ArrayList<Row> rows;
	
	public Cloud(int maxWidth, boolean d) {
		this.debugging = d;
		
		this.maxWidth = maxWidth;
		rows = new ArrayList<Row>(10);
	}
	
	public void add(Datum toAdd) {
		
		if (debugging) {
			System.out.printf("Attempting to add new Datum (%s) to current row...\n", toAdd.word);
		}
		
		if (rows.isEmpty() || !rows.get(0).add(toAdd)) {
			rows.add(0, new Row(maxWidth, debugging));
			rows.get(0).add(toAdd);
			
			if (debugging) {
				System.out.println("The current line was null or too full, so we added a new one!");
			}
			
		}
				
		if (debugging) {
			System.out.printf("Finished adding the new item!\nThe (new?) row's dimensions are (height: %d, width: %d)\n", rows.get(0).height, rows.get(0).width);
		}
	}
	
	public void calcSize() {
		for (Row r : rows) {
			width = Math.max(r.width, width);
			height += r.height;
		}
	}
	
}

class Datum implements Comparable<Datum> {
	
	int frequency, height, width;
	String word = "";
	
	public Datum(String word, int frequency) {
		this.word = word;
		this.frequency = frequency;
	}
	
	public void initSize(int maxFrequency) {
		height = (int) (8.0 + Math.ceil(40.0 * (frequency - 4.0) / (maxFrequency - 4.0)));
		width = (int) (Math.ceil(9.0 * word.length() * height / 16.0));
	}

	@Override
	public int compareTo(Datum other) {
		return word.compareTo(other.word);
	}
	
	@Override
	public String toString() {
		return "Word: " + word + ", Freq: " + frequency + ", Height: " + height + ", Width: " + width;
	}
	
}