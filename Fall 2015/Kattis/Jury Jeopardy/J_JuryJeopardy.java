package Contests.icpc.na.reg2015.warmup4;

import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class J_JuryJeopardy {
	
	static boolean debugging = true;
	static Scanner in;
	
	static final int EAST = 1, SOUTH = 2, WEST = 3, NORTH = 0;
	static int cases, direction, width, height, x, y, minX, maxX, minY, maxY;
	static char[] commands;
	static char[][] map;
	
	public static void main(String[] args) throws IOException {
		in = new Scanner(System.in);
		
		cases = in.nextInt();
		in.nextLine();
		
		System.out.println(cases);
		while (cases-- > 0) {
			if (debugging) {
				System.out.println("\n~~~~~~~~~~~~~~~~~~~~~~~~\n");
				System.out.printf("Cases left: %d\n", cases);
			}
			
			direction = EAST;
			x = 0;
			y = 100;
			map = new char[100][200];
			map[x][y] = '.';
			
			minX = 0; maxX = 0; minY = 100; maxY = 100;
			
			commands = in.nextLine().toCharArray();
			
			if (debugging) {
				System.out.println("Commands: " + Arrays.toString(commands));
			}
			
			for (char com : commands) {
				move(com);
			}
			
			width = maxX + 2;
			height = maxY - minY + 3;
			
			if (debugging) {
				System.out.printf("About to print map with the following info:\n\tMinY: %d\tMaxY: %d\n\tMinX: %d\t\tMaxX: %d\nWidth: %d\tHeight: %d\n", minY, maxY, minX, maxX, width, height);
			}
			
			printMap();
		}
		
		in.close();
	}
	
	public static void move(char command) {
		switch (command) {
		case 'F':
			if (debugging) {
				System.out.printf("Moving forward (%d)...\n", direction);
			}
			
			switch (direction) {
			case NORTH:
				y--;
				minY = Math.min(minY, y);
				break;
			case EAST:
				x++;
				maxX = Math.max(maxX, x);
				break;
			case SOUTH:
				y++;
				maxY = Math.max(maxY, y);
				break;
			case WEST:
				x--;
				minX = Math.min(minX, x);
				break;
			}
			break;
		case 'R':
			if (debugging) {
				System.out.println("Turning right...");
			}
			direction = (direction + 1) % 4;
			move('F');
			break;
		case 'B':
			if (debugging) {
				System.out.println("Reversing direction...");
			}
			direction = (direction + 2) % 4;
			move('F');
			break;
		case 'L':
			if (debugging) {
				System.out.println("Turning left...");
			}
			direction = (direction + 3) % 4;
			move('F');
			break;
		}
		
		map[x][y] = '.';
	}
	
	public static void printMap() {		
		System.out.printf("%d %d\n", height, width);
		
		for (int row = minY - 1; row <= maxY + 1; row++) {
			for (int col = minX; col <= maxX + 1; col++) {
				if (map[col][row] != '.') {
					System.out.print('#');
				} else {
					System.out.print(map[col][row]);
				}
			}
			System.out.println();
		}
	}
	
}
