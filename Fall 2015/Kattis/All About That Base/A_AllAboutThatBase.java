import java.io.IOException;
import java.util.Scanner;

public class A_AllAboutThatBase {
	
	static boolean debugging = false;
	static Scanner in;
	
	static int n;
	static String x, y, z, validBases;
	static boolean add, sub, mul, div;
	
	public static void main(String[] args) throws IOException {
		in = new Scanner(System.in);
		
		n = in.nextInt();
		in.nextLine();
		
		while (n-- > 0) {
			x = in.next();
			
			validBases = "";
			String temp = in.next();
			
			add = temp.indexOf('+') != -1;
			sub = temp.indexOf('-') != -1;
			mul = temp.indexOf('*') != -1;
			div = temp.indexOf('/') != -1;
			
			y = in.next();
			
			in.next();
			
			z = in.next();
			
			in.nextLine();
			
			if ((x + " - " + y + " = " + z).replaceAll("1", "").length() == 6) {
				boolean valid = true;
				
				double op1, op2, op3;
				
				op1 = x.length();
				op2 = y.length();
				op3 = z.length();
				
				if (add && valid) {
					valid = (op1 + op2 == op3);
				}
				
				if (sub) {
					valid = (op1 - op2 == op3);
				}
				
				if (mul) {
					valid = (op1 * op2 == op3);
				}
				
				if (div) {
					valid = (op1 / op2 == op3);
				}
				
				if (valid) {
					validBases += '1';
				}
			}
			
			for (int i = 2; i <= 36; i++) {
				boolean valid = true;
				
				if (debugging) {
					System.out.println("Currently checking base: " + i);
				}
				
				double op1 = -1, op2 = -1, op3 = -1;
				
				try {
					op1 = Long.parseLong(x, i);
					op2 = Long.parseLong(y, i);
					op3 = Long.parseLong(z, i);
				} catch (NumberFormatException e) {
					valid = false;
				}
				
				if (add && valid) {
					
					if (debugging) {
						System.out.println("Checking if " + op1 + " + " + op2 + " = " + op3 + "...");
					}
					
					valid = (op1 + op2 == op3);
				}
				
				if (sub && valid) {
					
					if (debugging) {
						System.out.println("Checking if " + op1 + " - " + op2 + " = " + op3 + "...");
					}
					
					valid = (op1 - op2 == op3);
				}
				
				if (mul && valid) {
					
					if (debugging) {
						System.out.println("Checking if " + op1 + " * " + op2 + " = " + op3 + "...");
					}
					
					valid = (op1 * op2 == op3);
				}
				
				if (div && valid) {
					
					if (debugging) {
						System.out.println("Checking if " + op1 + " / " + op2 + " = " + op3 + "...");
					}
					
					valid = (op1 / op2 == op3);
				}
				
				if (valid) {
					if (i > 9) {
						if (i == 36) {
							validBases += 0;
						} else {
							validBases += (char) (i - 10 + 'a');
						}
					} else {
						validBases += i;
					}
				}
			}
			
			if (validBases.length() == 0) {
				System.out.println("invalid");
			} else {
				System.out.println(validBases);
			}
			
		}
		
		in.close();
	}
	
}