import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class H_SecretMessage {
	
	static BufferedReader in;
	
	static int cases, l, n;
	static char[] table;
	static StringBuilder input, output;
	
	public static void main(String[] args) throws IOException {
		in = new BufferedReader(new InputStreamReader(System.in));
		
		cases = Integer.parseInt(in.readLine());
		
		while (cases-- > 0) {
			input = new StringBuilder(in.readLine());
			output = new StringBuilder();
			
			l = input.length();
			n = 1;
			
			while (n * n < l) {
				n++;
			}
			
			while (input.length() < n * n) {
				input.append('*');
			}
			
			table = input.toString().toCharArray();
			
			for (int i = 0; i < n; i++) {
				for (int j = table.length - n + i; j >= 0; j -= n) {
					if (table[j] != '*') {
						output.append(table[j]);
					}
				}
			}
			
			System.out.println(output);
		}
		
		in.close();
	}
	
}