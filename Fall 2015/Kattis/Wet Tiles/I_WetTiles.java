import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class I_WetTiles {
	
	static boolean debugging = false;
	static Scanner in;
	
	static int width, height, time, numLeak, numWall, tilesNeeded;
	static Tile[] leaks;
	static Tile[][] floor;
	
	public static void main(String[] args) throws IOException {
		in = new Scanner(System.in);
		
		while (initCase()) {			
			Queue<Tile> queue = new LinkedList<Tile>();
			
			for (Tile leak : leaks) {
				queue.add(leak);
			}
			
			while (!queue.isEmpty()) {
				Tile currentTile = queue.poll();
				int x = currentTile.x, y = currentTile.y, depth = currentTile.time;
				
				if (x < 0 || x >= width || y < 0 || y >= height || depth <= 0 || floor[x][y] != null) {
					continue;
				}
				
				tilesNeeded++;
				floor[x][y] = currentTile;
				queue.add(new Tile(x - 1, y, depth - 1));
				queue.add(new Tile(x + 1, y, depth - 1));
				queue.add(new Tile(x, y - 1, depth - 1));
				queue.add(new Tile(x, y + 1, depth - 1));				
			}
			
			if (debugging) {
				System.out.println("Floor after leaking:");
				printFloor();
				System.out.println("Case output:");
			}
			
			System.out.println(tilesNeeded);
			
		}
				
		in.close();
	}
	
	public static boolean initCase() {
		width = in.nextInt();
		
		if (width == -1) {
			return false;
		}
		
		height = in.nextInt();
		time = in.nextInt();
		numLeak = in.nextInt();
		numWall = in.nextInt();
		tilesNeeded = 0;
		
		leaks = new Tile[numLeak];
		floor = new Tile[width][height];
		
		for (int i = 0; i < numLeak; i++) {
			leaks[i] = new Tile (in.nextInt() - 1, in.nextInt() - 1, time);
		}
		
		if (debugging) {
			System.out.println("\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
			System.out.printf("Width: %d\tHeight: %d\nTime: %d\t\tLeaks: %d\tWalls: %d\n", width, height, time, numLeak, numWall);
			System.out.println("Leaks: " + Arrays.toString(leaks));
		}
		
		if (debugging) {
				System.out.println("\nBuilding walls...");
		}
		
		for (int i = 0; i < numWall; i++) {
			drawWall(in.nextInt() - 1, in.nextInt() - 1, in.nextInt() - 1, in.nextInt() - 1);
		}
		
		if (debugging) {
			System.out.println("Current floor:");
			printFloor();
			System.out.println();
		}
		
		return true;
	}
	
	public static void drawWall(int x1, int y1, int x2, int y2) {
		int x = x1, y = y1;
		int xinc = x2 - x1;
		xinc /= (x2 - x1) == 0 ? 1 : Math.abs(x2 - x1);
		int yinc = y2 - y1;
		yinc /= (y2 - y1) == 0 ? 1 : Math.abs(y2 - y1);
		
		if (debugging) {
			System.out.printf("Building wall from (%d,%d) to (%d,%d) with increments of %d and %d...\n", x1, y1, x2, y2, xinc, yinc);
		}
		
		floor[x][y] = new Tile(x, y, -1);
		
		while (x != x2 || y != y2) {
			x += xinc;
			y += yinc;
			
			floor[x][y] = new Tile(x, y, -1);
		}
	}
	
	public static void printFloor() {
		for (int i = height - 1; i >= 0; i--) {
			for (int j = 0; j < width; j++) {
				if (floor[j][i] == null) {
					System.out.print(" 000 ");
					continue;
				}
				
				if (floor[j][i].time == -1) {
					System.out.print("  X  ");
					continue;
				}
				
				System.out.printf(" %03d ", floor[j][i].time);
			}
			System.out.println();
		}
	}
	
}

class Tile {
	
	int x, y, time = Integer.MAX_VALUE;
	
	public Tile(int x, int y, int id) {
		time = id;		
		this.x = x;
		this.y = y;
	}
	
	@Override
	public String toString() {
		return "(x = " + x + ", y = " + y + ", time = " + time + ")";
	}
	
}
