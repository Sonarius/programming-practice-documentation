package Contests.icpc.na.reg2015.warmup6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;

public class G_EraseSecurely {
	
	static BufferedReader in;
	
	static int n;
	static BigInteger a, b;
	static String toPrint;
	
	public static void main(String[] args) throws IOException {
		in = new BufferedReader(new InputStreamReader(System.in));
				
		n = Integer.parseInt(in.readLine());
		a = new BigInteger(in.readLine(), 2);
		b = new BigInteger(in.readLine(), 2);
		
		toPrint = "Deletion ";
		
		if (n % 2 == 0) {
			toPrint += (a.equals(b)) ? "succeeded" : "failed";
		} else {
			for (int i = 0; i < a.bitLength(); i++) {
				a = a.flipBit(i);
			}
			toPrint += (a.equals(b)) ? "succeeded" : "failed";
		}
		
		System.out.println(toPrint);
		
		in.close();
	}
}
