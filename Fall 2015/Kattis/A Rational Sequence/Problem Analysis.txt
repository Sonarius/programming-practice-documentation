The tree described by the problem leaves about three distinct possibilites for the necessary traversals: the node is the right most node at its current depth, the node is the left-child of a parent node, the node is a right-child of a parent node.

For the first case, the pattern is pretty simple. The second case requires "traversing" up once, then right once. The last case involves "traversing" up to the first right-child node, then up once, then right once, then left by how ever many times you went up in the first part.

Thanks to the patterns created by the tree's rules, the "traversal" of nodes can be turned in to a constant time access with some arithmetic.

The solution can be turned in to an O(C) solution.