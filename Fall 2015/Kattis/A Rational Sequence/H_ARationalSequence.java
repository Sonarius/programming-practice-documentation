package Contests.icpc.na.reg2015.warmup5;

import java.io.IOException;
import java.util.Scanner;

public class H_ARationalSequence {
	
	static boolean debugging = false;
	static Scanner in;
	
	static int cases, caseNumber = 0, p, q;
	
	public static void main(String[] args) throws IOException {
		in = new Scanner(System.in);
		
		cases = in.nextInt();
		
		while (++caseNumber <= cases) {
			in.nextInt();
			Scanner tempS = new Scanner(in.nextLine().replace('/', ' '));
			p = tempS.nextInt();
			q = tempS.nextInt();
			tempS.close();
			
			if (debugging) {
				System.out.println("\n~~~~~~~~~~~~~~~~~~~~~~~~\n");
				System.out.printf("Case %d:\nFinding pair after %d/%d\n", caseNumber, p, q);
			}
			
			nextPair();
			
			System.out.println(caseNumber + " " + p + "/" + q);
		}
		
		in.close();
	}
	
	public static void nextPair() {
		if (q == 1) {
			
			if (debugging) {
				System.out.println("Right most node at current depth -> First node at next depth");
			}
			
			q = p + 1;
			p = 1;
		} else if (p / q == 0) {
			
			if (debugging) {
				System.out.println("Node is a left child -> Go up once right, then down right");
			}
			
			q -= p;
			p += q;			
		} else {
			
			if (debugging) {
				System.out.printf("Node is right child -> Go up %d times then up left once, then down right once, then left %d times again\n", (p - (p % q)) / q, (p - (p % q)) / q);
			}
			
			search();
		}
	}
	
	public static void search() {
		int pt, qt, depth;
		
		depth = p / q;
		
		pt = p % q;
		qt = q;
		
		qt -= pt;
		pt += qt;
		
		qt += (pt * depth);
		
		p = pt;
		q = qt;
	}
	
}
