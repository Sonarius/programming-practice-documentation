package Contests.icpc.na.reg2015.warmup6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

public class A_PlantingTrees {
	
	static boolean debugging = false;
	static BufferedReader in;
	static StringTokenizer st;
	
	static int n, days;
	static int[] trees;
	
	public static void main(String[] args) throws IOException {
		in = new BufferedReader(new InputStreamReader(System.in));
		
		n = Integer.parseInt(in.readLine());
		st = new StringTokenizer(in.readLine());
		
		trees = new int[n];
		for (int i = 0; i < n; i++) {
			trees[i] = Integer.parseInt(st.nextToken());
		}
		
		Arrays.sort(trees);
		
		if (debugging) {
			System.out.printf("%d Trees to plant\n", n);
			System.out.println("Trees:\n" + Arrays.toString(trees));
		}
		
		trees[n - 1] += 2;
		
		for (int i = n - 2; i >= 0; i--) {
			trees[i] = Math.max(trees[i + 1], trees[i] + (n - i + 1));
		}
		
		if (debugging) {
			System.out.println("Min Days:\n" + Arrays.toString(trees));
		}
		
		System.out.println(trees[0]);
		
		in.close();
	}
	
}
