
#include <bits/stdc++.h>
using namespace std;

const int MAX_INT = std::numeric_limits<int>::max();
const int MIN_INT = std::numeric_limits<int>::min();
const int INF = 1.0e9;
const int NEG_INF = -1.0e9;

#define MEM(arr,val)memset(arr,val, sizeof arr)
#define PI acos(0)*2.0
#define eps 1.0e-9
#define are_equal(a,b)fabs(a-b)<eps
#define LS(b)(b& (-b)) // Least significant bit
#define DEG_to_RAD(a)((a*PI)/180.0) // convert to radians

#define forn(i,n) for(int (i) = 0; (i) < (n); (i)++)
#define fornm(i,n,m) for(int (i) = (n); (i) < (m); (i)++)
#define dprintf if(debugging)printf

#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef pair<ii,int> iii;
typedef pair<int,char> ic;
typedef pair<long,char> lc;
typedef vector<int> vi;
typedef vector<ii> vii;

int gcd(int a,int b){return b == 0 ? a : gcd(b,a%b);}
int lcm(int a,int b){return a*(b/gcd(a,b));}
int cmp(const void *a, const void *b) {return *(int*)a - *(int*)b;}

const int debugging = 0;

const int N = 9;

string s;
int n, m, caseNum = 1;
int level[N], term[1 << N], vva[N];

int traverse() {
	// Traversal will be replaced with a binary search
	
	int r = (1 << n) - 1, l = 0, m = (l + r) / 2; // Init range vars
	
	dprintf("r: %d, l: %d, m: %d\n", r, l, m);
	
	forn(i,n) { // For each level
		if (vva[level[i]]) { // If current level is 1
			// Go right
			l = m + 1;
			m = (l + r) / 2;
		} else { // Current level must be 0
			// Go left
			r = m;
			m = (l + r) / 2;
		}
	}
	
	return term[m];
}

void printTreeLevels() {
	cout << "Tree " << caseNum << " Levels: " << endl;
	forn(i,n)
		cout << " " << level[i];
	cout << endl;
}

int main() {
	FASTER;
	
	cin >> n;
	
	while (n) {
		
		cout << "S-Tree #" << caseNum	<< ":" << endl;
		
		forn(i,n) { // For each tree level
			cin >> s;
			level[i] = s[1] - '0' - 1; // Parse which x is this level
		}
		
		if (debugging) printTreeLevels();
		
		// Read terminal node values into num array
		cin >> s;
		forn(i, 1 << n)
			term[i] = s[i] - '0';
		
		// Read VVA's
		cin >> m;
		forn(i,m) { //  For each VVA
			// Read each VVA into int array
			cin >> s;
			forn(j,n)
				vva[j] = s[j] - '0';
			
			// Print result for this VVA
			cout << traverse();
		}
		
		// Add whitespace for current case
		cout << endl << endl;
		
		// Prep for next case
		caseNum++;
		cin >> n;
	}
	
	return 0;
}

