
#include <bits/stdc++.h>
using namespace std;

const int MAX_INT = std::numeric_limits<int>::max();
const int MIN_INT = std::numeric_limits<int>::min();
const int INF = 1.0e9;
const int NEG_INF = -1.0e9;

#define MEM(arr,val)memset(arr,val, sizeof arr)
#define PI acos(0)*2.0
#define eps 1.0e-9
#define are_equal(a,b)fabs(a-b)<eps
#define LS(b)(b& (-b)) // Least significant bit
#define DEG_to_RAD(a)((a*PI)/180.0) // convert to radians

#define forn(i,n) for(int (i) = 0; (i) < (n); (i)++)
#define fornm(i,n,m) for(int (i) = (n); (i) < (m); (i)++)
#define dprintf if(debugging)printf

#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef pair<ii,int> iii;
typedef pair<int,char> ic;
typedef pair<long,char> lc;
typedef vector<int> vi;
typedef vector<ii> vii;

int gcd(int a,int b){return b == 0 ? a : gcd(b,a%b);}
int lcm(int a,int b){return a*(b/gcd(a,b));}

const int debugging = 1;

int primes[] = {2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97};

int main() {
	FASTER;
	
	int num_primes = sizeof(primes) / sizeof(int);
	int num, pow, base;
	int pows[num_primes + 1];
	
	while (1) {
	   cin >> num;
	   if (num < 1) break;
	   MEM(pows,0); // Reset counters
	   
	   forn(i,num_primes) { // Count power of each prime base
	      base = primes[i];
	      pow = base;
	      while ((num / pow) > 0) {
	         pows[i] += (num / pow);
	         pow *= base;
	      }
	   }
	   
	   printf("%3d! =", num);
	   forn(i,num_primes) {
	      if (pows[i] == 0) break;
	      
	      if (!((i+1) % 16)) printf("\n      ");
	      
	      printf("%3d", pows[i]);
	   }
	   printf("\n");
	}
	
	return 0;
}

