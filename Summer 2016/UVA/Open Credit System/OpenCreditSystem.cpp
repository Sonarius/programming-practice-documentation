
#include <bits/stdc++.h>
using namespace std;

const int MAX_INT = std::numeric_limits<int>::max();
const int MIN_INT = std::numeric_limits<int>::min();
const int INF = 1.0e9;
const int NEG_INF = -1.0e9;

#define MEM(arr,val)memset(arr,val, sizeof arr)
#define PI acos(0)*2.0
#define eps 1.0e-9
#define are_equal(a,b)fabs(a-b)<eps
#define LS(b)(b& (-b)) // Least significant bit
#define DEG_to_RAD(a)((a*PI)/180.0) // convert to radians

#define forn(i,n) for(int (i) = 0; (i) < (n); (i)++)
#define fornm(i,n,m) for(int (i) = (n); (i) < (m); (i)++)
#define dprintf if(debugging)printf

#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef pair<ii,int> iii;
typedef pair<int,char> ic;
typedef pair<long,char> lc;
typedef vector<int> vi;
typedef vector<ii> vii;

int gcd(int a,int b){return b == 0 ? a : gcd(b,a%b);}
int lcm(int a,int b){return a*(b/gcd(a,b));}
int cmp(const void *a, const void *b) {return *(int*)a - *(int*)b;}

const int debugging = 1;

int main() {
	FASTER;
	
	int T,x,ans;
	cin >> T;
	
	while (T--) {
      cin >> x;
   	int a[x], _min[x], _max[x];
   	
   	forn(i,x) // Clear arrays
   	   a[i] = _min[i] = _max[i] = 0;
   	
   	forn(i,x) // Init input values
   	   cin >> a[i];
   	
   	// Do base cases for max and min
   	_max[0] = a[0];
   	_min[x-1] = a[x-1];
   	
   	// Calc max and min for each index
   	fornm(i,1,x) {
   	   _max[i] = max(_max[i-1],a[i]);
   	   _min[x-i-1] = min(_min[x-i],a[x-i-1]);
   	}
   	
   	// Find greatest diff across max and min
   	ans = NEG_INF;
   	forn(i,x-1)
   	   ans = max(ans,_max[i]-_min[i+1]);
   	cout << ans << endl;
   }
	
	return 0;
}

