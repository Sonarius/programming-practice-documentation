#include <bits/stdc++.h>
using namespace std;
 
const int MAX_INT = std::numeric_limits<int>::max();
const int MIN_INT = std::numeric_limits<int>::min();
const int INF = 1.0e9;
const int NEG_INF = -1.0e9;
 
#define MEM(arr,val)memset(arr,val, sizeof arr)
#define PI acos(0)*2.0
#define eps 1.0e-9
#define are_equal(a,b)fabs(a-b)<eps
#define LS(b)(b& (-b)) // Least significant bit
#define DEG_to_RAD(a)((a*PI)/180.0) // convert to radians
 
#define forn(i,n) for(int (i) = 0; (i) < (n); (i)++)
#define fornm(i,n,m) for(int (i) = (n); (i) < (m); (i)++)
#define dprintf if(debugging)printf
 
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)
 
typedef long long ll;
typedef pair<int,int> ii;
typedef pair<ii,int> iii;
typedef pair<int,char> ic;
typedef pair<long,char> lc;
typedef vector<int> vi;
typedef vector<ii> vii;
 
int gcd(int a,int b){return b == 0 ? a : gcd(b,a%b);}
int lcm(int a,int b){return a*(b/gcd(a,b));}
int cmp(const void *a, const void *b) {return *(int*)a - *(int*)b;}
 
const int debugging = 1;

const int N = 300000;

int n;
int dist[N], sc[N];

queue<int> q;

void bfs() {
	dist[0] = 0;
	
	// First neighbor
	dist[1] = 1;
	q.push(1);
	
	// First shortcut
	if (sc[0] != 0) {
		dist[sc[0]] = 1;
		q.push(sc[0]);
	}
	
	while (!q.empty()) {
		int u = q.front();
		q.pop();
		int d = dist[u] + 1;
		
		// Next neighbor
		if (u + 1 < n && d < dist[u + 1]) {
			dist[u + 1] = d;
			q.push(u + 1);
		}
		
		// Prev neighbor
		if (u - 1 >= 0 && d < dist[u - 1]) {
			dist[u - 1] = d;
			q.push(u - 1);
		}
		
		// Shortcut
		if (d < dist[sc[u]]) {
			dist[sc[u]] = d;
			q.push(sc[u]);
		}
	}
	
}

int main() {
	FASTER;
	
	cin >> n;
	forn(i,n) {
		cin >> sc[i];
		sc[i]--;
		dist[i] = INF;
	}
	
	bfs();
	
	forn(i,n) cout << dist[i] << " ";
	
	return 0;
}
