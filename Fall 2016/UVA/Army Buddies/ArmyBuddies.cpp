
#include <bits/stdc++.h>
using namespace std;

const int MAX_INT = std::numeric_limits<int>::max();
const int MIN_INT = std::numeric_limits<int>::min();
const int INF = 1.0e9;
const int NEG_INF = -1.0e9;

#define MEM(arr,val)memset(arr,val, sizeof arr)
#define PI acos(0)*2.0
#define eps 1.0e-9
#define are_equal(a,b)fabs(a-b)<eps
#define LS(b)(b& (-b)) // Least significant bit
#define DEG_to_RAD(a)((a*PI)/180.0) // convert to radians

#define forn(i,n) for(int (i) = 0; (i) < (n); (i)++)
#define fornm(i,n,m) for(int (i) = (n); (i) < (m); (i)++)
#define formn(i,m,n) for(int (i) = (m); (i) > (n); (i)--)
#define DEBUG if(debugging)

#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef pair<ii,int> iii;
typedef pair<int,char> ic;
typedef pair<long,char> lc;
typedef vector<int> vi;
typedef vector<ii> vii;

int gcd(int a,int b){return b == 0 ? a : gcd(b,a%b);}
int lcm(int a,int b){return a*(b/gcd(a,b));}
int cmp(const void *a, const void *b) {return *(int*)a - *(int*)b;}

const int debugging = 0;

const int S = 1.0e5+5;

int s, b, l, r, depl[S], depr[S];
bool alive[S];

int par(int i, int d) {
	DEBUG printf("\tpar(%d,%d)...\n",i,d);
	if (d && depl[i] < 0) return -1;
	if (!d && depr[i] < 0) return -1;
	bool a = d ? alive[depl[i]] : alive[depr[i]];
	if (a) return (d ? depl[i] : depr[i]);
	return (d ? (depl[i]=par(depl[i],d)) : (depr[i]=par(depr[i],d)));
}

int main() {
	FASTER;
	
	cin >> s >> b;

	while (s+b) {
		forn(i,S) alive[i] = true;
		fornm(i,1,S) depl[i] = i-1;
		formn(i,S,-1) depr[i] = i + 1;
		depl[1] = depr[s] = -1;

		DEBUG {
			printf("Arrays:\nL: ");
			fornm(i,1,s+1) printf("%d, ",depl[i]);
			printf("\nR: ");
			fornm(i,1,s+1) printf("%d, ",depr[i]);
			printf("\nA: ");
			fornm(i,1,s+1) printf("%d, ",alive[i] ? 1 : 0);
			printf("\n");
		}

		while (b--) {
			DEBUG printf("S: %d; B: %d\n",s,b);

			cin >> l >> r;
			fornm(i,l,r+1) alive[i] = false;

			DEBUG {
				printf("Processing [%d,%d]...\n",l,r);
				printf("Arrays:\nL: ");
				fornm(i,1,s+1) printf("%d, ",depl[i]);
				printf("\nR: ");
				fornm(i,1,s+1) printf("%d, ",depr[i]);
				printf("\nA: ");
				fornm(i,1,s+1) printf("%d, ",alive[i] ? 1 : 0);
				printf("\n");
			}

			int pl = par(l,1), pr = par(r,0);
			if (pl < 0) cout << "* ";
			else cout << pl << " ";
			if (pr < 0) cout << "*";
			else cout << pr;
			cout << endl;
		}

		cout << "-" << endl;
		cin >> s >> b;
	}
	
	return 0;
}
