
#include <bits/stdc++.h>
using namespace std;

const int MAX_INT = std::numeric_limits<int>::max();
const int MIN_INT = std::numeric_limits<int>::min();
const int INF = 1.0e9;
const int NEG_INF = -1.0e9;

#define MEM(arr,val)memset(arr,val, sizeof arr)
#define PI acos(0)*2.0
#define eps 1.0e-9
#define are_equal(a,b)fabs(a-b)<eps
#define LS(b)(b& (-b)) // Least significant bit
#define DEG_to_RAD(a)((a*PI)/180.0) // convert to radians

#define forn(i,n) for(int (i) = 0; (i) < (n); (i)++)
#define fornm(i,n,m) for(int (i) = (n); (i) < (m); (i)++)
#define formn(i,m,n) for(int (i) = (m); (i) > (n); (i)--)
#define DEBUG if(debugging)

#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef pair<ii,int> iii;
typedef pair<int,char> ic;
typedef pair<long,char> lc;
typedef vector<int> vi;
typedef vector<ii> vii;

int gcd(int a,int b){return b == 0 ? a : gcd(b,a%b);}
int lcm(int a,int b){return a*(b/gcd(a,b));}
int cmp(const void *a, const void *b) {return *(int*)a - *(int*)b;}

const int debugging = 0;

const int N = 9999;

int na, nb, r1, r2, r3, r4;
string l;
char a[N][N];
char b[N][N];

bool foo(int x, int y, int r) {
	if (x+nb > na || y+nb > na) return false;
	
	if (r==1) { // 0 deg
		fornm(i,x,x+nb)
			fornm(j,y,y+nb)
				if (a[i][j]!=b[i-x][j-y]) return false;
	} else if (r==2) { // 90 deg
		fornm(i,x,x+nb)
			fornm(j,y,y+nb)
				if (a[i][j]!=b[j-y][nb-(i-x)-1]) return false;
	} else if (r==3) { // 180 deg
		fornm(i,x,x+nb)
			fornm(j,y,y+nb)
				if (a[i][j]!=b[nb-(i-x)-1][nb-(j-y)-1]) return false;
	} else { // 270 deg
		fornm(i,x,x+nb)
			fornm(j,y,y+nb){
				DEBUG cout << "Comparing " << i-x << "," << j-y << " to " << nb-(j-y)-1 << "," << i-x << endl;
				if (a[i][j]!=b[nb-(j-y)-1][i-x]) return false;}
	}

	return true;
}

int main() {
	FASTER;
	
	cin >> na >> nb;

	while (na+nb) {
		DEBUG cout << "A:" << endl;
		forn(i,na) {
			cin >> l;
			forn(j,na) a[j][i] = l[j];

			DEBUG {
				forn(j,na) cout << a[j][i] << " ";
				cout << endl;
			}
		}

		DEBUG cout << "B:" << endl;
		forn(i,nb) {
			cin >> l;
			forn(j,nb) b[j][i] = l[j];

			DEBUG {
				forn(j,na) cout << b[j][i] << " ";
				cout << endl;
			}
		}

		r1=r2=r3=r4=0;
		forn(i,na) forn(j,na) {
			r1 += foo(i,j,1) ? 1 : 0;
			r2 += foo(i,j,2) ? 1 : 0;
			r3 += foo(i,j,3) ? 1 : 0;
			r4 += foo(i,j,4) ? 1 : 0;
		}
		cout << r1 << " " << r2 << " " << r3 << " " << r4 << endl;
		cin >> na >> nb;
	}
	
	return 0;
}
