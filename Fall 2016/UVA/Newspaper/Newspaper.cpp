
// This problem uses unsigned chars which will overflow regular chars in C++
// Adding 128 to the read char will place chars with value 0-127 in the second
// half of the array, and the regular overflowed chars will occupy the first
// half.

#include <bits/stdc++.h>
using namespace std;

const int MAX_INT = std::numeric_limits<int>::max();
const int MIN_INT = std::numeric_limits<int>::min();
const int INF = 1.0e9;
const int NEG_INF = -1.0e9;

#define MEM(arr,val)memset(arr,val, sizeof arr)
#define PI acos(0)*2.0
#define eps 1.0e-9
#define are_equal(a,b)fabs(a-b)<eps
#define LS(b)(b& (-b)) // Least significant bit
#define DEG_to_RAD(a)((a*PI)/180.0) // convert to radians

#define forn(i,n) for(int (i) = 0; (i) < (n); (i)++)
#define fornm(i,n,m) for(int (i) = (n); (i) < (m); (i)++)
#define dprintf if(debugging)printf

#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef pair<ii,int> iii;
typedef pair<int,char> ic;
typedef pair<long,char> lc;
typedef vector<int> vi;
typedef vector<ii> vii;

int gcd(int a,int b){return b == 0 ? a : gcd(b,a%b);}
int lcm(int a,int b){return a*(b/gcd(a,b));}
int cmp(const void *a, const void *b) {return *(int*)a - *(int*)b;}

const int debugging = 1;

int cases, chars, lines, cost[256], len;
double res, p;
char q, line[10005];

int main() {
	FASTER;
	
	scanf("%d",&cases);

	while (cases--) {
		forn(i,256) cost[i] = 0;

		scanf("%d",&chars);

		forn(i,chars) {
			scanf(" %c %lf",&q,&p);
			cost[q+128] = p;
		}

		scanf("%d",&lines); getchar();

		res = 0.0;

		forn(i,lines) {
			gets(line); len = strlen(line);
			forn(j,len) res += cost[line[j]+128];
		}
		
		res /= 100.0;
		printf("%.2lf$\n",res);
	}
	
	return 0;
}

