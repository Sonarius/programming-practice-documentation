
#include <bits/stdc++.h>
using namespace std;

const int MAX_INT = std::numeric_limits<int>::max();
const int MIN_INT = std::numeric_limits<int>::min();
const int INF = 1.0e9;
const int NEG_INF = -1.0e9;

#define MEM(arr,val)memset(arr,val, sizeof arr)
#define PI acos(0)*2.0
#define eps 1.0e-9
#define are_equal(a,b)fabs(a-b)<eps
#define LS(b)(b& (-b)) // Least significant bit
#define DEG_to_RAD(a)((a*PI)/180.0) // convert to radians

#define forn(i,n) for(int (i) = 0; (i) < (n); (i)++)
#define fornm(i,n,m) for(int (i) = (n); (i) < (m); (i)++)
#define dprintf if(debugging)printf

#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef pair<ii,int> iii;
typedef pair<int,char> ic;
typedef pair<long,char> lc;
typedef vector<int> vi;
typedef vector<ii> vii;

int gcd(int a,int b){return b == 0 ? a : gcd(b,a%b);}
int lcm(int a,int b){return a*(b/gcd(a,b));}
int cmp(const void *a, const void *b) {return *(int*)a - *(int*)b;}

const int debugging = 1;

const int N = 105, M = N*(N-1)/2, T = 12;
ll a[T][N];
vi e[N];

int n, m, s, t, u, v;
ll r;

void foo() {
	forn(i,t+1)
		forn(j,n)
			a[i][j] = 0;
	
	a[0][s] = 1;

	forn(i,t+1)
		forn(j,n)
			forn(k,e[j].size())
				a[i+1][e[j][k]] += a[i][j];

	r = 0;
	forn(i,n) r += a[t][i];

}

int main() {
	FASTER;
	
	cin >> n >> m >> s >> t;

	forn(i,m) {
		cin >> u >> v;
		e[u].push_back(v);
		e[v].push_back(u);
	}

	foo();

	cout << r << endl;
	
	return 0;
}

