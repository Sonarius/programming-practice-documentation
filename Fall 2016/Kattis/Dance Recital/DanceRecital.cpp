
#include <bits/stdc++.h>
using namespace std;

const int MAX_INT = std::numeric_limits<int>::max();
const int MIN_INT = std::numeric_limits<int>::min();
const int INF = 1.0e9;
const int NEG_INF = -1.0e9;

#define MEM(arr,val)memset(arr,val, sizeof arr)
#define PI acos(0)*2.0
#define eps 1.0e-9
#define are_equal(a,b)fabs(a-b)<eps
#define LS(b)(b& (-b)) // Least significant bit
#define DEG_to_RAD(a)((a*PI)/180.0) // convert to radians

#define forn(i,n) for(int (i) = 0; (i) < (n); (i)++)
#define fornm(i,n,m) for(int (i) = (n); (i) < (m); (i)++)
#define dprintf if(debugging)printf

#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef pair<ii,int> iii;
typedef pair<int,char> ic;
typedef pair<long,char> lc;
typedef vector<int> vi;
typedef vector<ii> vii;

int gcd(int a,int b){return b == 0 ? a : gcd(b,a%b);}
int lcm(int a,int b){return a*(b/gcd(a,b));}
int cmp(const void *a, const void *b) {return *(int*)a - *(int*)b;}

const int debugging = 0;

const int MAX_RECITALS = 10;
int p[MAX_RECITALS][MAX_RECITALS];

int r;
string rec[MAX_RECITALS];
bool v[MAX_RECITALS];

int rec_dist(string a, string b) {
	int res = 0;
	int ia = 0, ib = 0;

	while (ia < a.length() && ib < b.length()) {
		if (a[ia] == b[ib]) {
			ia++; ib++; res++;
		} else {
			if (a[ia] < b[ib]) ia++;
			else ib++;
		}
	}
	
	if (debugging) cout << "Cost of " << a << " to " << b << " = " << res << endl;

	return res;
}

int dfs(int start, int curr, int cost, int n) {
	if (n == r) return cost;

	v[curr] = true;
	int res = INF;
	forn(i,r)
		if (i != curr && !v[i])
			res = min(res,dfs(start,i,cost+p[curr][i],n+1));
	v[curr] = false;

	return res;
}

int main() {
	FASTER;
	
	cin >> r;
	forn(i,r) cin >> rec[i];

	forn(i,r)
		fornm(j,i+1,r)
			p[j][i] = p[i][j] = rec_dist(rec[i],rec[j]);
	
	int res = INF;
	forn(i,r)
		res = min(res,dfs(i,i,0,1));
	cout << res << endl;
		
	
	return 0;
}
