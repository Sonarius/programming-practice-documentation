import java.util.*;
import java.io.*;

class AClassyProblem {
	StringTokenizer st;
	BufferedReader in;
	StringBuilder out;

	int t,p;
	String l,n,tok;
	StringBuilder c;
	ArrayList<Data> data;

	public AClassyProblem() throws IOException {
		foo();
		//try {
		//	foo();
		//} catch (IOException ioe) {
		//	System.out.println(ioe.getMessage());
		//} catch (NumberFormatException nfe) {
		//	System.out.println(nfe.getMessage());
		//	System.out.printf("\nFAILED TO SCAN STRING: \"%s\"\n",l);
		//}
	}

	public void foo() throws IOException {
		in = new BufferedReader(new InputStreamReader(System.in));
		out = new StringBuilder();
		
		l = in.readLine();
		t = Integer.parseInt(l);

		while (t-- > 0) {
			l = in.readLine();
			p = Integer.parseInt(l);

			data = new ArrayList<Data>(p);
			for(int i = 0; i < p; i++) {
				l = in.readLine();
				st = new StringTokenizer(l,":- \n");

				n = st.nextToken();
				c = new StringBuilder();

				while (st.hasMoreTokens()) {
					tok = st.nextToken();
					if (tok.charAt(0) == 'c') break;

					c.append(tok.charAt(0));
				}

				c.reverse();
				while(c.length()<15) c.append('m');

				data.add(new Data(n,c.toString()));
			}

			Collections.sort(data);
			for(Data d:data) {out.append(d.n);out.append("\n");}
			out.append("==============================\n");
		}

		System.out.println(out);
	}

	public static void main(String[] args) throws IOException {
		new AClassyProblem();
	}
}

class Data implements Comparable<Data> {
	public String n, c;

	public Data(String n, String c) {
		this.n = n;
		this.c = c;
	}

	public int compareTo(Data other) {
		if (this.c.compareTo(other.c) == 0) return this.n.compareTo(other.n);
		return -this.c.compareTo(other.c);
	}
}
