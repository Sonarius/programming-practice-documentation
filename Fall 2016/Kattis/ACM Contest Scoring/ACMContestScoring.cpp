

#include <bits/stdc++.h>
using namespace std;

const int MAX_INT = std::numeric_limits<int>::max();
const int MIN_INT = std::numeric_limits<int>::min();
const int INF = 1.0e9;
const int NEG_INF = -1.0e9;

#define MEM(arr,val)memset(arr,val, sizeof arr)
#define PI acos(0)*2.0
#define eps 1.0e-9
#define are_equal(a,b)fabs(a-b)<eps
#define LS(b)(b& (-b)) // Least significant bit
#define DEG_to_RAD(a)((a*PI)/180.0) // convert to radians

#define forn(i,n) for(int (i) = 0; (i) < (n); (i)++)
#define fornm(i,n,m) for(int (i) = (n); (i) < (m); (i)++)
#define dprintf if(debugging)printf

#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef pair<ii,int> iii;
typedef pair<int,char> ic;
typedef pair<long,char> lc;
typedef vector<int> vi;
typedef vector<ii> vii;

int gcd(int a,int b){return b == 0 ? a : gcd(b,a%b);}
int lcm(int a,int b){return a*(b/gcd(a,b));}
int cmp(const void *a, const void *b) {return *(int*)a - *(int*)b;}

const int debugging = 0;

const int MAX_QUESTIONS = 105;
int questions[MAX_QUESTIONS];
int minute;
char question;
string line, verdict;

int main() {
	FASTER;
	
	forn(i,MAX_QUESTIONS) questions[i] = 0;
	
	cin >> minute;	

	while (minute != -1) {
		cin >> question >> verdict;
		question  -= 'A';

		if (questions[question] <= 0) {
			questions[question]--;
			if (verdict[0] == 'r') {
				questions[question]++;
				questions[question] *= -20;
				questions[question] += minute;

				if (debugging) cout << "question " << (char) (question + 'A') << ": right with time " << questions[question] << endl;
			}
		}

		cin >> minute;
	}

	int q = 0, m = 0;
	forn(i,MAX_QUESTIONS) {
		if (questions[i] > 0) {
			q++;
			m += questions[i];
		}
	}

	cout << q << " " << m << endl;
	
	return 0;
}

