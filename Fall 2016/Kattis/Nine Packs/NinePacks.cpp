
#include <bits/stdc++.h>
using namespace std;

const int MAX_INT = std::numeric_limits<int>::max();
const int MIN_INT = std::numeric_limits<int>::min();
const int INF = 1.0e9;
const int NEG_INF = -1.0e9;

#define MEM(arr,val)memset(arr,val, sizeof arr)
#define PI acos(0)*2.0
#define eps 1.0e-9
#define are_equal(a,b)fabs(a-b)<eps
#define LS(b)(b& (-b)) // Least significant bit
#define DEG_to_RAD(a)((a*PI)/180.0) // convert to radians

#define forn(i,n) for(int (i) = 0; (i) < (n); (i)++)
#define fornm(i,n,m) for(int (i) = (n); (i) < (m); (i)++)
#define formn(i,m,n) for(int (i) = (m); (i) > (n); (i)--)
#define dprintf if(debugging)printf

#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef pair<ii,int> iii;
typedef pair<int,char> ic;
typedef pair<long,char> lc;
typedef vector<int> vi;
typedef vector<ii> vii;

int gcd(int a,int b){return b == 0 ? a : gcd(b,a%b);}
int lcm(int a,int b){return a*(b/gcd(a,b));}
int cmp(const void *a, const void *b) {return *(int*)a - *(int*)b;}

const int debugging = 0;

const int MAX_DOGS = 120000;

int H, B;
int h[105], b[105];
int dp[2][MAX_DOGS][105];

void fill_dp() {
	
	// Init dp to INF then dp(0,y) and dp(h[0] and b[0],1) to 1
	forn(i,MAX_DOGS) { fill(dp[0][i],dp[0][i]+105,INF); fill(dp[1][i],dp[1][i]+105,INF); }
	forn(i,max(H,B)) dp[0][0][i] = dp[1][0][i] = 0;
	dp[0][h[1]][1] = dp[1][b[1]][1] = 1;
	
	// Init remaining rows
	int curr_max_dogs = 0;
	fornm(i,1,H+1) { // Try using the first ith packets
		curr_max_dogs += h[i];
		fornm(j,1,curr_max_dogs+1) { // To make j dogs
			int _j = j-h[i];
			int inc_cost = _j < 0 ? INF : dp[0][_j][i-1] + 1;
			int exc_cost = dp[0][j][i-1];
			
			if (debugging) cout << "Deciding between " << inc_cost << " & " << exc_cost << " for " << j << ", " << i << endl;
			
			dp[0][j][i] = min(inc_cost,exc_cost);
		}
	}
	
	curr_max_dogs = 0;
	fornm(i,1,B+1) { // Try using the first ith packets
		curr_max_dogs += b[i];
		fornm(j,1,curr_max_dogs+1) { // To make j dogs
			int _j = j-b[i];
			int inc_cost = _j < 0 ? INF : dp[1][_j][i-1] + 1;
			int exc_cost = dp[1][j][i-1];
			
			if (debugging) cout << "Deciding between " << inc_cost << " & " << exc_cost << " for " << j << ", " << i << endl;
			
			dp[1][j][i] = min(inc_cost,exc_cost);
		}
	}
	
	if (debugging) {
		cout << "Last rows: ";
		fornm(i,1,37) cout << dp[0][i][H] << "\t" << dp[1][i][B] << endl;
	}
}

int min_pack() {
	int res = INF;
	
	fornm(i,1,MAX_DOGS) {
		if (debugging && res > (dp[0][i][H] + dp[1][i][B])) cout << "Updating res to " << dp[0][i][H] + dp[1][i][B] << " at i=" << i << endl;
		res = min(res,dp[0][i][H]+dp[1][i][B]);
	}
	
	return res;
}

int main() {
	FASTER;
	
	cin >> H;
	forn(i,H) cin >> h[i+1];
	cin >> B;
	forn(i,B) cin >> b[i+1];
	
	fill_dp();
	
	int ans = min_pack();
	
	if (ans < INF) cout << ans;
	else cout << "impossible";
	cout << endl;
	
	return 0;
}

