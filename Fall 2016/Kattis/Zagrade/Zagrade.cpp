#include <bits/stdc++.h>
using namespace std;
 
const int MAX_INT = std::numeric_limits<int>::max();
const int MIN_INT = std::numeric_limits<int>::min();
const int INF = 1.0e9;
const int NEG_INF = -1.0e9;
 
#define MEM(arr,val)memset(arr,val, sizeof arr)
#define PI acos(0)*2.0
#define eps 1.0e-9
#define are_equal(a,b)fabs(a-b)<eps
#define LS(b)(b& (-b)) // Least significant bit
#define DEG_to_RAD(a)((a*PI)/180.0) // convert to radians
 
#define forn(i,n) for(int (i) = 0; (i) < (n); (i)++)
#define fornm(i,n,m) for(int (i) = (n); (i) < (m); (i)++)
#define dprintf if(debugging)printf
 
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)
 
typedef long long ll;
typedef pair<int,int> ii;
typedef pair<ii,int> iii;
typedef pair<int,char> ic;
typedef pair<long,char> lc;
typedef vector<int> vi;
typedef vector<ii> vii;
 
int gcd(int a,int b){return b == 0 ? a : gcd(b,a%b);}
int lcm(int a,int b){return a*(b/gcd(a,b));}
int cmp(const ii&a, const ii&b) {return a.first >= b.first;}
 
const int debugging = 0;

const int MAX_PAR = 10, MAX_EXPR_LEN = 200;

int perm;
string expr;
stack<ii> par_stk;
vii pars;
vector<string> perms;

int main() {
	FASTER;
	
	cin >> expr;
	
	// Parse parenthesis pair queue and ordered list
	forn(i,expr.length()) {
		if (expr[i] == '(') {
			par_stk.push(make_pair(i,-1));
		} else if (expr[i] == ')') {
			ii par = par_stk.top(); par_stk.pop();
			par.second = i;
			pars.push_back(par);
		}
	}
	
	sort(pars.begin(), pars.end(), cmp);
	
	if (debugging) {forn(i,pars.size())	cout << " " << pars[i].first; cout << endl;}
	
	// Add each permutation (non-unique)
	perm = (1 << (pars.size())) - 2;
	
	while(perm > -1) {
		string expr_ = expr;
		string permS;
		
		if (debugging) cout << "testing perm(" << perm << ")...\n";
		
		forn(i,pars.size()) { // Lets check with pairs we include
			if (debugging) cout << "\ttesting pair (" << pars[i].first << "," << pars[i].second << ")...\n";
			if (perm & (1 << i)) { // Do we include this pair?
				// Do nothing
			} else { // Replace this pair's chars with trash
				expr_[pars[i].first] = '.';
				expr_[pars[i].second] = '.';
			}
		}
		
		// Append non-trash characters
		forn(i,expr_.length()) if (expr_[i] != '.') permS += expr_[i];
		perms.push_back(permS);
		
		perm--;
	}
	
	// Print unique permutations
	sort(perms.begin(), perms.end());
	forn(i,perms.size()) {
		if (i == 0 || perms[i].compare(perms[i - 1]) != 0)
			cout << perms[i] << endl;
	}
	
	return 0;
}