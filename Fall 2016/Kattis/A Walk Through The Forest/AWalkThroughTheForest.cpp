
#include <bits/stdc++.h>
using namespace std;

const int MAX_INT = std::numeric_limits<int>::max();
const int MIN_INT = std::numeric_limits<int>::min();
const int INF = 1.0e9;
const int NEG_INF = -1.0e9;

#define MEM(arr,val)memset(arr,val, sizeof arr)
#define PI acos(0)*2.0
#define eps 1.0e-9
#define are_equal(a,b)fabs(a-b)<eps
#define LS(b)(b& (-b)) // Least significant bit
#define DEG_to_RAD(a)((a*PI)/180.0) // convert to radians

#define forn(i,n) for(int (i) = 0; (i) < (n); (i)++)
#define fornm(i,n,m) for(int (i) = (n); (i) < (m); (i)++)
#define dprintf if(debugging)printf

#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef pair<ii,int> iii;
typedef pair<int,char> ic;
typedef pair<long,char> lc;
typedef vector<int> vi;
typedef vector<ii> vii;

int gcd(int a,int b){return b == 0 ? a : gcd(b,a%b);}
int lcm(int a,int b){return a*(b/gcd(a,b));}
int cmp(const void *a, const void *b) {return *(int*)a - *(int*)b;}

const int debugging = 0;

const int N = 2000, M = N*N + N;
long p[N], c[N];
vii e[N];

int n, m, u, v, w;

void bfs() {
	int source = 1;

	queue<int> q;
	p[source] = 1; c[source] = 0;
	q.push(source);

	while (!q.empty()) {
		u = q.front(); q.pop();
		vii edg = e[u];

		forn(i,edg.size()) {
			v = edg[i].first;
			w = edg[i].second;

			if (c[v] > w + c[u]) {
				c[v] = w + c[u];
				p[v] = 1;
				q.push(v);
			}else if (c[v] == w + c[u]) {
				p[v]++;
				q.push(v);
			}
		}
	}
}

int main() {
	FASTER;
	
	cin >> n;
	while (n) {
		cin >> m;

		// Read edges
		forn(i,m) {
			cin >> u >> v >> w;
			e[u].push_back(ii(v,w));
			e[v].push_back(ii(u,w));
		}

		// Clear path counts
		forn(i,n+1) p[i] = 0;

		// Init costs
		forn(i,n+1) c[i] = INF;

		bfs();

		cout << p[2] << endl;

		// Clear edges
		forn(i,m+1) e[i].clear();

		cin >> n;
	}
	
	return 0;
}

