
#include <bits/stdc++.h>
using namespace std;

const int MAX_INT = std::numeric_limits<int>::max();
const int MIN_INT = std::numeric_limits<int>::min();
const int INF = 1.0e9;
const int NEG_INF = -1.0e9;

#define MEM(arr,val)memset(arr,val, sizeof arr)
#define PI acos(0)*2.0
#define eps 1.0e-9
#define are_equal(a,b)fabs(a-b)<eps
#define LS(b)(b& (-b)) // Least significant bit
#define DEG_to_RAD(a)((a*PI)/180.0) // convert to radians

#define forn(i,n) for(int (i) = 0; (i) < (n); (i)++)
#define fornm(i,n,m) for(int (i) = (n); (i) < (m); (i)++)
#define dprintf if(debugging)printf

#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef pair<ii,int> iii;
typedef pair<int,char> ic;
typedef pair<long,char> lc;
typedef vector<int> vi;
typedef vector<ii> vii;

int gcd(int a,int b){return b == 0 ? a : gcd(b,a%b);}
int lcm(int a,int b){return a*(b/gcd(a,b));}
int cmp(const void *a, const void *b) {return *(int*)a - *(int*)b;}

const int debugging = 0;

int n, e;
bool visited[200];
int value[200];
vii ver;
vii children[200];

void bfs() {
	queue<int> q;
	ver[n].second = 0;
	value[n] = ver[n].first;
	
	q.push(n);
	while (!q.empty()) {
		int u = q.front(); q.pop();
		
		if (debugging) cout << "Visiting " << u << "..." << endl;
		
		forn(i,children[u].size()) {
			ii _child = children[u][i];
			int v = _child.first;
			
			int cost = ver[u].second + _child.second;
			if (debugging) cout << "Proposed cost of visitng " << v << ": " << cost << "; Curr cost: " << ver[v].second << endl;
			
			int count = value[u] + ver[v].first;
			
			if (cost <= ver[v].second) {
				if (cost < ver[v].second) {
					if (debugging) cout << "Updating " << v << " to " << cost << ", " << count << endl;
					ver[v].second = cost;
					value[v] = count;
				} else if (count > value[v]) {
					if (debugging) cout << "Updating " << v << " to " << cost << ", " << count << endl;
					value[v] = count;
				}

				q.push(v);
			}
		}
	}
}

int main() {
	FASTER;
	
	ver.push_back(ii(-INF,INF));
	
	cin >> n;
	forn(i,n) {
		int val; cin >> val;
		ver.push_back(ii(val,INF));
		visited[i+1] = false;
		value[i+1] = -INF;
	}
	
	cin >> e;
	forn(i,e) {
		int source, sink, weight;
		cin >> source >> sink >> weight;
		
		children[source].push_back(ii(sink,weight));
		children[sink].push_back(ii(source,weight));
		
		if (debugging) cout << "Added edge " << source << ", " << sink << ", " << weight << endl;
	}
	
	bfs();
	
	if (ver[1].second == INF) {
		cout << "impossible" << endl;
		return 0;
	}
	
	cout << ver[1].second << " " << value[1] << endl;
	
	return 0;
}

